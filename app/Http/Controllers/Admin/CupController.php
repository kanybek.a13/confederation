<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Cup;
use App\Models\Federation;
use Illuminate\Http\Request;

class CupController extends Controller
{
    public function index(Federation $federation)
    {
        $allCups = Cup::whereNotNull('id');

        if (\request('name')!=''){
            $allCups = $allCups->where('name', 'ILIKE' , '%'. \request('name') . '%');
        }

        $allCups = $allCups->orderBy('id')
            ->paginate(10);

        return view('admin.cup.index', [
            'allCups' => $allCups,
            'currentFederation' => $federation,
        ]);
    }

    public function create(Federation $federation)
    {
        return view('admin.cup.new', [
            'currentFederation' => $federation,
        ]);
    }

    public function store(Request $request, Federation $federation)
    {
        $attributes = $this->validateCoachInfo($request);
        $request->validate([
            'image' => 'required:jpg,jpeg,bmp,png',
        ]);

        if ($request->hasFile('image')) {
            $image = $request->file('image');

            $path = public_path(). '/assets/img/cups';
            $filename = $image->GetClientOriginalName();

            $image->move($path, $filename);

            $cup= Cup::create([
                'name' => $attributes['name'],
                'position' => $attributes['position'],
                'federation_id' => $attributes['federation_id'],
                'image' => $path . '/' . $filename
            ]);

            return redirect(route('admin.cup.show', [$federation->site, $cup->id]));
        }

        return redirect()->back();
    }

    public function show(Federation $federation, Cup $cup)
    {
        return view('admin.cup.show', [
            'cup'=> $cup,
            'currentFederation' => $federation,
        ]);
    }

    public function edit(Federation $federation, Cup $cup)
    {
        return view('admin.cup.edit', [
            'cup' => $cup,
            'currentFederation' => $federation,
        ]);
    }

    public function update(Federation $federation, Cup $cup)
    {
        $attributes = $this->validateCoachInfo(\request());

        $cup->update([
            'name' => $attributes['name'],
            'title' => $attributes['title'],
        ]);

        return redirect(route('admin.cup.show', [$federation->site, $cup->id]));
    }

    public function destroy(Federation $federation, Cup $cup)
    {
        $cup->delete();

        return redirect()->back();
    }

    public function validateCoachInfo($data){
        return request()->validate([
            'name' => 'required|string',
            'title' => 'required|string',
        ]);
    }
}
