<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Event;
use App\Models\Federation;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class EventController extends Controller
{
    public function index(Federation $federation)
    {
        $event = Event::where('federation_id', $federation->id);

        if (\request('name')!=''){
            $event = $event->where('name', 'ILIKE' , '%'. \request('name') . '%');
        }

        $event = $event->orderBy('id')
            ->paginate(10);

        return view('admin.event.index', [
            'events' => $event,
            'currentFederation' => $federation,
        ]);
    }

    public function show(Federation $federation, Event $event)
    {
        return view('admin.event.show', [
            'event'=> $event,
            'currentFederation' => $federation,
        ]);
    }

    public function edit(Federation $federation, Event $event)
    {
        return view('admin.event.edit', [
            'event' => $event,
            'currentFederation' => $federation,
        ]);
    }

    public function create(Federation $federation)
    {
        return view('admin.event.new', [
            'currentFederation' => $federation,
        ]);
    }

    public function store(Request $request, Federation $federation)
    {
        $attributes = $this->validateEventsInfo($request);

        $request->validate([
            'file' => 'required:jpg,jpeg,bmp,png',
        ]);

        if ($request->hasFile('file')) {
            $file = $request->file('file');

            $path = public_path() . '/assets/img/events';

            $filename = $file->GetClientOriginalName();

            $file->move($path, $filename);

            $event = Event::create([
                'name' => $attributes['name'],
                'slug' => Str::slug($attributes['name']),
                'start_date' => $attributes['start_date'],
                'finish_date' => $attributes['finish_date'],
                'location' => $attributes['location'],
                'path' => $path . '/' . $filename,
                'federation_id' => $federation->id,
            ]);

            return redirect(route('admin.events', [$federation->site]));
        }

        return redirect()->back();
    }

    public function update(Federation $federation, Event $event)
    {
        $attributes = $this->validateEventsInfo(\request());

        $event->update($attributes);

        return redirect(route('admin.event.show', [$federation->site, $event->id]));
    }

    public function destroy(Federation $federation, Event $event)
    {
        $event->delete();

        return redirect(route('admin.events', [$federation->site]));
    }

    public function validateEventsInfo($data){
        return request()->validate([
            'name' => 'required',
            'start_date' => 'required',
            'finish_date' => 'required',
            'location' => 'required',
            'federation_id' => 'required'
        ]);
    }}
