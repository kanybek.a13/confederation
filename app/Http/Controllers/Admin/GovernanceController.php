<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Governance;
use App\Models\Federation;
use Illuminate\Http\Request;

class GovernanceController extends Controller
{
    public function index(Federation $federation)
    {
        $governances = Governance::where('federation_id', $federation->id);

        if (\request('fullname')!=''){
            $governances = $governances->where('fullname', 'ILIKE' , '%'. \request('fullname') . '%');
        }

        $governances = $governances->orderBy('id')
            ->paginate(10);

        return view('admin.governance.index', [
            'governances' => $governances,
            'currentFederation' => $federation,
        ]);
    }

    public function create(Federation $federation)
    {
        return view('admin.governance.new', [
            'currentFederation' => $federation,
        ]);
    }

    public function store(Request $request, Federation $federation)
    {
        $attributes = $this->validateInfo($request);
        $request->validate([
            'image' => 'required:jpg,jpeg,bmp,png',
        ]);

        if ($request->hasFile('image')) {
            $image = $request->file('image');

            $path = public_path(). '/assets/img/governance';
            $filename = $image->GetClientOriginalName();

            $image->move($path, $filename);

            $governance = Governance::create([
                'fullname' => $attributes['fullname'],
                'position' => $attributes['position'],
                'federation_id' => $attributes['federation_id'],
                'image' => $path . '/' . $filename
            ]);

            return redirect(route('admin.governance.show', [$federation->site, $governance->id]));
        }

        return redirect()->back();
    }

    public function show(Federation $federation, Governance $governance)
    {
        return view('admin.governance.show', [
            'governance'=> $governance,
            'currentFederation' => $federation,
        ]);
    }

    public function edit(Federation $federation, Governance $governance)
    {
        return view('admin.governance.edit', [
            'governance' => $governance,
            'currentFederation' => $federation,
        ]);
    }

    public function update(Federation $federation, Governance $governance)
    {
        $attributes = $this->validateInfo(\request());

        $governance->update([
            'fullname' => $attributes['fullname'],
            'position' => $attributes['position'],
            'federation_id' => $attributes['federation_id']
        ]);

        return redirect(route('admin.governance.show', [$federation->site, $governance->id]));
    }

    public function destroy(Federation $federation, Governance $governance)
    {
        $governance->delete();

        return redirect()->back();
    }

    public function validateInfo($data){
        return request()->validate([
            'fullname' => 'required|string',
            'position' => 'required|string',
            'federation_id' => 'required|integer',
        ]);
    }
}
