<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Federation;
use App\Models\Structure;
use Illuminate\Http\Request;

class StructureController extends Controller
{
    public function index(Federation $federation)
    {
        $structure = Structure::where('federation_id', $federation->id);

        return view('admin.structure.index', [
            'structure' => $structure,
            'currentFederation' => $federation,
        ]);
    }

    public function create(Federation $federation)
    {
        return view('admin.structure.new', [
            'currentFederation' => $federation,
        ]);
    }

    public function store(Request $request, Federation $federation)
    {
        $attributes = $this->validateInfo($request);
        $request->validate([
            'image' => 'required:jpg,jpeg,bmp,png',
        ]);

        if ($request->hasFile('image')) {
            $image = $request->file('image');

            $path = public_path(). '/assets/img/structures';
            $filename = $image->GetClientOriginalName();

            $image->move($path, $filename);

            $structure= Structure::create([
                'title' => $attributes['title'],
                'body' => $attributes['body'],
                'federation_id' => $federation->id,
                'image' => $path . '/' . $filename
            ]);

            return redirect(route('admin.structure', [$federation->site]));
        }

        return redirect()->back();
    }

    public function show(Federation $federation)
    {
        return view('admin.structure.show', [
            'structure' => $federation->structure,
            'currentFederation' => $federation,
        ]);
    }

    public function edit(Federation $federation)
    {
        return view('admin.structure.edit', [
            'structure' => $federation->structure,
            'currentFederation' => $federation,
        ]);
    }

    public function update(Federation $federation)
    {
        $attributes = $this->validateInfo(\request());

        $federation->structure->update([
            'title' => $attributes['title'],
            'body' => $attributes['body'],
            'federation_id' => $federation->id
        ]);

        return redirect(route('admin.structure', [$federation->site]));
    }

    public function destroy(Federation $federation)
    {
        $federation->structure->delete();

        return redirect(route('admin.structure', [$federation->site]));
    }

    public function validateInfo($data){
        return request()->validate([
            'title' => 'required',
            'body' => 'required',
        ]);
    }}
