<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Role;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    public function index(){
        $users = User::whereNotNull('id');

        if (request('name')) {
            $users = $users->where('name', 'ILike', '%' . request('name') . '%');
        }

        if (request('email')) {
            $users = $users->where('email', 'ILike', '%' . request('email') . '%');
        }

        $users = $users->latest('updated_at')
            ->paginate(5);

        return view('admin.user.index', [
            'users' => $users
        ]);
    }

    public function show(User $user){
        return view('admin.user.show', [
            'user' => $user,
        ]);
    }

    public function edit(User $user){
        return view('admin.user.edit', [
            'user'=> $user,
            'user_role' => $user->roles[0],
            'roles' => Role::all()->sortBy('name')
        ]);
    }

    public function update(User $user){
        $validatedData = $this->validateUserInfo(\request());

        $user->update([
            'name' => $validatedData['name'],
            'email' => $validatedData['email'],
            'password' => Hash::make($validatedData['password'])
        ]);

        $detaches = $user->roles()->pluck('id')->toArray();
        $user->roles()->detach($detaches);

        $role = $validatedData['role'];
        $user->assignRole($role);

        return redirect('/admin-panel/users');
    }

    public function delete(User $user){
        $user->delete();

        return redirect('/admin-panel/users');
    }

    public function new(){
        return view('admin.user.new',[
            'roles' => Role::all()->sortBy('name')
        ]);
    }

    public function store(){
        $validatedData = $this->validateUserInfo(\request());

        $user = User::create([
            'name' => $validatedData['name'],
            'email' => $validatedData['email'],
            'password' => Hash::make($validatedData['password']),
        ]);

        $user->assignRole($validatedData['role']);

        return redirect('/admin-panel/users');
    }

    public function validateUserInfo(){
        return request()->validate([
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255'],
            'password' => ['required', 'string', 'confirmed'],
            'role'=>'required',
        ]);
    }
}

