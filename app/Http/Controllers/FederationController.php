<?php

namespace App\Http\Controllers;

use App\Models\Athlete;
use App\Models\City;
use App\Models\Coach;
use App\Models\Cup;
use App\Models\Event;
use App\Models\Federation;
use App\Models\Media;
use App\Models\News;
use App\Models\Partner;
use App\Models\Result;
use App\Models\Section;
use App\Models\Sity;
use App\Models\Sport;
use App\Models\Team;
use App\Models\TeamMember;
use Illuminate\Http\Request;

class FederationController extends Controller
{
    public function index(Federation $federation)
    {
        return view('index', [
            'currentFederation' => $federation,
            'governances' => $federation->governance()->take(2)->get(),
            'last_news' => $federation->news()
                ->latest('created_at')
                ->take(7)
                ->get(),
        ]);
    }

    public function news(Federation $federation)
    {
        $last_news = News::where('federation_id', $federation->id);

        if (\request('search') != ''){
            $last_news = $last_news->where('title', 'ILIKE' , '%'. \request('search') . '%')
                ->orWhere('body', 'ILIKE' , '%'. \request('search') . '%');
        }

        $last_news = $last_news->latest('created_at')
            ->paginate(10);

        return view('news.index',  [
            'currentFederation' => $federation,
            'last_news' => $last_news
        ]);
    }

    public function cup(Federation $federation, Cup $cup)
    {
        return view('cup',  [
            'currentFederation' => $federation,
            'cup' => $cup,
            'results' => json_decode($cup->results),
            'images' => json_decode($cup->images)
        ]);
    }

    public function newsShow(Federation $federation, News $news)
    {
        return view('news.show',  [
            'currentFederation' => $federation,
            'news' => $news
        ]);
    }

    public function governance(Federation $federation)
    {
        return view('governance', [
            'currentFederation' => $federation,
            'governances' => $federation->governance()->get()
        ]);
    }

    public function apparatus(Federation $federation)
    {
        $apparatuses = $federation->apparatus()->get();

        return view('apparatus', [
            'currentFederation' => $federation,
            'apparatuses' => $apparatuses,
        ]);
    }

    public function history(Federation $federation)
    {
        return view('history',[
            'currentFederation' => $federation,
            'history' => $federation->history()->get()->first()
        ]);
    }

    public function media(Federation $federation)
    {
        $medias = Media::where('federation_id', $federation->id);

        if (\request('search') != ''){
            $medias = $medias->where('name', 'ILIKE' , '%'. \request('search') . '%');
        }

        $medias = $medias->paginate(8);

        return view('media.index', [
            'currentFederation' => $federation,
            'medias' => $medias
        ]);
    }

    public function mediaShow(Federation $federation, Media $media)
    {
        return view('media.photo-gallery', [
            'currentFederation' => $federation,
            'media' => $media,
            'photos' => $media->photos
        ]);
    }

    public function goals(Federation $federation)
    {
        return view('goals', [
            'currentFederation' => $federation,
        ]);
    }

    public function article(Federation $federation)
    {
        return view('news.article', [
            'currentFederation' => $federation,
        ]);
    }

    public function coaches(Federation $federation)
    {
        $teams = $federation->teams;

        if (\request('team_id')){
            $team = Team::where('id', \request('team_id'))->first();

            $coaches= $team->coaches;
        }else
            $coaches = $teams[0]->coaches;

        return view('coach.index', [
            'currentFederation' => $federation,
            'teams' => $teams,
            'coaches' => $coaches
        ]);
    }

    public function coachShow(Federation $federation, Coach $coach)
    {
        return view('coach.show', [
            'currentFederation' => $federation,
            'coach' => $coach
        ]);
    }

    public function interview(Federation $federation)
    {
        return view('interview', [
            'currentFederation' => $federation,
        ]);
    }

    public function massmedia(Federation $federation)
    {
        return view('media.massmedia', [
            'currentFederation' => $federation,
        ]);
    }

    public function events(Federation $federation)
    {
        $events = Event::where('federation_id', $federation->id);

        if (\request('search') != ''){
            $events = $events->where('name', 'ILIKE' , '%'. \request('search') . '%')
                ->orWhere('location', 'ILIKE' , '%'. \request('search') . '%')
                ->orWhere('about', 'ILIKE' , '%'. \request('search') . '%');
        }

        $events = $events->paginate(4);

        return view('event.index', [
            'currentFederation' => $federation,
            'events' => $events
        ]);
    }

    public function eventShow(Federation $federation, Event $event)
    {
        return view('event.show', [
            'currentFederation' => $federation,
            'event' => $event
        ]);
    }

    public function results(Federation $federation)
    {
        $results = Result::where('federation_id', $federation->id);

        if (\request('search') != ''){
            $results = $events->where('name', 'ILIKE' , '%'. \request('search') . '%');
        }

        return view('results', [
            'currentFederation' => $federation,
            'results' => $results,
        ]);
    }

    public function teams(Federation $federation)
    {
        $teams = $federation->teams;

        if (\request('team_id')){
            $team = Team::where('id', \request('team_id'))->first();

            $teamAthletes = $team->athletes;
        }else
            $teamAthletes = $teams[0]->athletes;

        return view('teams', [
            'currentFederation' => $federation,
            'teams' => $teams,
            'teamAthletes' => $teamAthletes
        ]);
    }

    public function athlete(Federation $federation, Team $team, Athlete $athlete)
    {
        return view('athlete', [
            'currentFederation' => $federation,
            'team' => $team,
            'athlete' => $athlete
        ]);
    }

    public function structure(Federation $federation)
    {
        return view('structure', [
            'currentFederation' => $federation,
            'structure' => $federation->structure,
        ]);
    }

    public function searchResult(Request $request, Federation $federation)
    {
        return view('search-results', [
            'currentFederation' => $federation,
        ]);
    }

    public function sections(Federation $federation, Request $request)
    {
        $federation_id = $request->input('federation_id');

        if ($federation_id && $federation_id != 'all'){
            $sections = Section::where('federation_id', $federation_id);
        }elseif ($federation_id === 'all'){
           $sections = Section::whereNotNull('federation_id');
        }else
            $sections = Section::where('federation_id', $federation->id);

        if ($name = $request->input('name')){
            $sections = $sections->where('name', 'LIKE', "%{$name}%");
        }

        if ($address = $request->input('address')){
            $sections = $sections->where('address', 'LIKE', "%{$address}%");
        }

        if ($city_id = $request->input('city_id')){
            $sections = $sections->where('city_id', $city_id);
        }

        $sections = $sections->paginate(10);

        return view('section.index', [
            'currentFederation' => $federation,
            'cities' => City::all()->sortBy('name'),
            'sections' => $sections
        ]);
    }

    public function sectionShow(Federation $federation, Section $section)
    {
        return view('section.show', [
            'currentFederation' => $federation,
            'section' => $section
        ]);
    }
}
