<?php

namespace App\Http\View\Composers;

use App\Models\Cup;
use Illuminate\View\View;

class CupComposer
{
    public function compose(View $view){
        $view->with('cups', Cup::all());
    }
}
