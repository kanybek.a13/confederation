<?php

namespace App\Http\View\Composers;

use App\Models\Partner;
use Illuminate\View\View;

class PartnerComposer
{
    public function compose(View $view){
        $view->with('partners', Partner::all());
    }
}
