<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
    use HasFactory;

    protected $fillable = ['name', 'slug', 'start_date', 'finish_date', 'location', 'federation_id', 'about'];

    public function federation(){
        return $this->belongsTo(Federation::class, 'federation_id', 'id');
    }
}
