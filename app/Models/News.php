<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\File;

class News extends Model
{
    use HasFactory;

    protected $fillable = ['title', 'body', 'federation_id'];

    public function path()
    {
        return "/news/{$this->id}";
    }

    public function files()
    {
        return $this->belongsToMany(File::class,'news_files');
    }

    public function assignFile($file)
    {
        $this->files()->attach($file->id);
    }

    public function federation()
    {
        return $this->belongsTo(Federation::class);
    }
}
