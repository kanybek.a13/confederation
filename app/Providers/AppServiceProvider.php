<?php

namespace App\Providers;

use App\Http\ViewComposers\CupComposer;
use App\Http\ViewComposers\PartnerComposer;
use App\Models\Cup;
use App\Models\Federation;
use App\Models\Partner;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
