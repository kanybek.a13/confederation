<?php

namespace App\Providers;

use App\Http\View\Composers\CupComposer;
use App\Http\View\Composers\PartnerComposer;
use App\Models\Federation;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class ComposerServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        #1. To all
        View::share('allFederations', Federation::all());
//        View::share('partners', Partner::all());
//        View::share('cups', Cup::all());
//
//        View::composer('layouts.header', function ($view){
//            $view->with('allFederations', Federation::all());
//        });

        #2. To identified views
//        View::composer('layouts.header', function ($view){
//            $view->with('cups', Cup::all());
//        });
//
//        View::composer('layouts.partners-section', function ($view){
//            $view->with('partners', Partner::all());
//        });

        #3. With class
        View::composer('layouts.app', PartnerComposer::class);
        View::composer('layouts.header', CupComposer::class);    }
}
