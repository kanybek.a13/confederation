<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAthletesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('athletes', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('fullname');
            $table->string('birthplace');
            $table->date('date_of_birth');
            $table->integer('weight');
            $table->integer('rating');
            $table->text('about');
            $table->string('image');
            $table->unsignedBigInteger('federation_id');

            $table->foreign('federation_id')
                ->references('id')
                ->on('federations')
                ->onDelete('cascade');

            $table->timestamps();
        });

        Schema::create('athlete_team', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->unsignedBigInteger('athlete_id');
            $table->unsignedBigInteger('team_id');

            $table->foreign('athlete_id')
                ->references('id')
                ->on('athletes')
                ->onDelete('cascade');

            $table->foreign('team_id')
                ->references('id')
                ->on('teams')
                ->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('athletes');
        Schema::dropIfExists('athlete_team');
    }
}
