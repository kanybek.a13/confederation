<?php

namespace Database\Seeders;

use App\Models\Apparatus;
use Illuminate\Database\Seeder;

class ApparatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Apparatus::create([
            'fullname' => 'Ельдос Рамазанов',
            'position' => 'Генеральный директор',
            'position_notes' => 'Руководство',
            'email' => 'finance@confederation.kz',
            'phone' => '+7 (7172) 280-994',
            'image' => 'http://confederation.local/assets/img/apparatus/1.jpg',
            'federation_id' => 8,
        ]);

        Apparatus::create([
            'fullname' => 'Нурлан Ногербек',
            'position' => 'Заместитель Генерального директора',
            'position_notes' => 'Руководство',
            'email' => 'finance@confederation.kz',
            'phone' => '+7 (7172) 280-994',
            'image' => 'http://confederation.local/assets/img/apparatus/2.jpg',
            'federation_id' => 8,
        ]);

        Apparatus::create([
            'fullname' => 'Финансовый директор',
            'position' => 'Финансовый директор',
            'position_notes' => 'Служба финансового обеспечения',
            'email' => 'finance@confederation.kz',
            'phone' => '+7 (7172) 280-993',
            'image' => 'http://confederation.local/assets/img/empty-human.jpg',
            'federation_id' => 8,
        ]);

        Apparatus::create([
            'fullname' => 'Главный бухгалтер',
            'position' => 'Главный бухгалтер',
            'position_notes' => 'Служба финансового обеспечения',
            'email' => 'accountant@confederation.kz',
            'phone' => '+7 (7172) 280-993',
            'image' => 'http://confederation.local/assets/img/apparatus/2.jpg',
            'federation_id' => 8,
        ]);

        Apparatus::create([
            'fullname' => 'Олжабаева Ксения Болатовна',
            'position' =>  'Координатор',
            'position_notes' => 'Служба правового обеспечения',
            'email' => 'sport_law@confederation.kz',
            'phone' => '+7 (7172) 280-993',
            'image' => 'http://confederation.local/assets/img/apparatus/5.jpg',
            'federation_id' => 8,
        ]);

        Apparatus::create([
            'fullname' => 'Кужасарина Арайлым Жанатовна',
            'position' => 'Юрист',
            'position_notes' => 'Служба правового обеспечения',
            'email' => 'kuzhassarina@confederation.kz',
            'phone' => '+7 (7172) 280-993',
            'image' => 'http://confederation.local/assets/img/apparatus/6.jpg',
            'federation_id' => 8,
        ]);

        Apparatus::create([
            'fullname' => 'Нугманов Ахат Бахитжанович',
            'position_notes' => 'Служба по работе со спортивными организациями',
            'position' => 'Менеджер по спорту',
            'email' => 'nugmanov@confederation.kz',
            'phone' => '+7 (7172) 280-993',
            'image' => 'http://confederation.local/assets/img/apparatus/5.jpg',
            'federation_id' => 8,
        ]);
    }
}
