<?php

namespace Database\Seeders;

use App\Models\Governance;
use Illuminate\Database\Seeder;

class GovernanceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Governance::create([
            'fullname' => 'Абулгазин Данияр Рустемович',
            'federation_id' => 1,
            'position' => 'Председатель Конфедерации,
Председатель исполнительного комитета',
            'image' => '/assets/img/governance/1.jpg',
        ]);

        Governance::create([
            'fullname' => 'Бектенов Бекжан Мухтасифович',
            'federation_id' => 1,
            'position' => 'Исполнительный директор ОЮЛ в ФА "Казахстанская федерация бокса",
Член исполнительного комитета',
            'image' => '/assets/img/governance/2.jpg',
        ]);

        Governance::create([
            'fullname' => 'Мынбаев Сауат Мухаметбаевич',
            'federation_id' => 1,
            'position' => 'Президент РОО «Федерация греко-римской, вольной и женской борьбы»,
Вице-председатель Конфедерации, Член исполнительного комитета',
            'image' => '/assets/img/governance/3.jpg',
        ]);

        Governance::create([
            'fullname' => 'Ким Вячеслав Константинович',
            'federation_id' => 1,
            'position' => 'Президент РОО «Федерация Таеквондо (WTF) Республики Казахстан»,
Вице-председатель Конфедерации, Член исполнительного комитета',
            'image' => '/assets/img/governance/4.jpg',
        ]);

        Governance::create([
            'fullname' => 'Тусупбеков Жанат Рашидович',
            'federation_id' => 1,
            'position' => 'Президент ОО «Федерация тяжелой атлетики» (республиканское),
Вице-председатель Конфедерации, Член исполнительного комитета',
            'image' => '/assets/img/governance/5.jpg',
        ]);
    }
}
