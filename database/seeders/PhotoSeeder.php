<?php

namespace Database\Seeders;

use App\Models\Photo;
use Illuminate\Database\Seeder;

class PhotoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Photo::create([
            'name' => 'test photo media',
            'path' => '/assets/img/photos/1.jpg',
            'media_id' => '1',
        ]);

        Photo::create([
            'name' => 'test photo media',
            'path' => '/assets/img/photos/2.jpg',
            'media_id' => '1',
        ]);

        Photo::create([
            'name' => 'test photo media',
            'path' => '/assets/img/photos/3.jpg',
            'media_id' => '1',
        ]);

        Photo::create([
            'name' => 'test photo media',
            'path' => '/assets/img/photos/4.jpg',
            'media_id' => '1',
        ]);


        Photo::create([
            'name' => 'test photo media',
            'path' => '/assets/img/photos/2.jpg',
            'media_id' => '1',
        ]);
    }
}
