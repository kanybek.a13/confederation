<?php

namespace Database\Seeders;

use App\Models\Ability;
use App\Models\Role;
use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $admin_ability = Ability::firstOrCreate([
            'name' => 'admin',
            'slug' => 'admin ',
        ]);

        $edit_ability = Ability::firstOrCreate([
            'name' => 'edit_content',
            'slug' => 'edit_content',
        ]);

        $view_ability = Ability::firstOrCreate([
            'name' => 'view_content',
            'slug' => 'view_content',
        ]);

//    --------------------------------------------------------------------------

        $admin_role = Role::firstOrCreate([
            'name' => 'admin',
            'slug' => 'admin role',
        ]);

//    --------------------------------------------------------------------------

        $admin_role->allowTo($admin_ability);
        $admin_role->allowTo($edit_ability);
        $admin_role->allowTo($view_ability);

//    --------------------------------------------------------------------------

        $aidar = User::create([
            'name' => 'Aidar',
            'email' => 'kanybek.a13@gmail.com',
            'email_verified_at' => now(),
            'password' => Hash::make('aidar'),
        ]);

        $admin = User::create([
            'name' => 'admin_name',
            'email' => 'admin@gmail.com',
            'email_verified_at' => now(),
            'password' => Hash::make('admin'),
        ]);

//    --------------------------------------------------------------------------

        $aidar->assignRole($admin_role);
        $admin->assignRole($admin_role);

    }
}
