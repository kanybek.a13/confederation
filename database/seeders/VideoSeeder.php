<?php

namespace Database\Seeders;

use App\Models\Video;
use Illuminate\Database\Seeder;

class VideoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Video::create([
            'name' => 'test photo media',
            'source' => 'https://www.youtube.com/watch?v=eA_CWh_GF6Y',
            'media_id' => '2',
        ]);
    }
}
