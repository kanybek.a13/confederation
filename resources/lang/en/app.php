<?php
return [
    'about_federation'=> 'About Federation',
    'governance'=> 'Governance',
    'structure'=> 'Structure of Federation',
    'history' => 'History of Federation',
    'documents' => 'Documents',
    'federations' => 'Federations',
    'news'=> 'News',
    'media'=> 'Media',
    'confederation_cup'=> 'Confederation Cup',
    'events'=> 'Events',
    'results'=> 'Results',
    'teams'=> 'National teams',
    'coaches'=> 'Coaching staff',
    'sections'=> 'Sections',
    'contacts'=> 'Contacts',
    'main' => 'MAIN',
    'sponsors' => 'Sponsors and partners',
];

