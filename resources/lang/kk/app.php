<?php
return [
    'about_federation'=> 'Федерация жайлы',
    'governance'=> 'Басшылық',
    'structure'=> 'Федерация құрылымы',
    'history' => 'Федерация тарихы',
    'documents' => 'Құжаттар',
    'federations' => 'Федерациялар',
    'news'=> 'Жаңалықтар',
    'media'=> 'БАҚ',
    'confederation_cup'=> 'Конфедерация кубогы',
    'events'=> 'Іс-шаралар күнтізбесі',
    'results'=> 'Нәтижелер',
    'teams'=> 'Ұлттық құрамалар',
    'coaches'=> 'Бапкерлер құрамы',
    'sections'=> 'Секциялар',
    'contacts'=> 'Байланыс',
    'main' => 'БАСТЫ',
    'sponsors' => 'Демеушілер және серіктестер',
];
