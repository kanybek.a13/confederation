@include('admin.layouts.header')

<main class="main">
    <div class="container container-fluid">
        <br>
        <ul class="breadcrumbs">
            <li><span>{{$currentFederation->name}}</span></li>
            <li><a href="{{ route('admin.athletes', [$currentFederation->site]) }}" title="Атлеты">Атлеты</a></li>
        </ul>
    </div>
    <div class="container ">
        <section>
            <h2 class="title-primary"> Атлеты</h2>

            <form action="{{ route('admin.athletes', [$currentFederation->site]) }}" method="GET">
                <div class="row row--multiline">
                    <div class="col-md-3 col-sm-6">
                        <div class="input-group">
                            <label class="input-group__title">Название</label>
                            <input type="text" name="name" class="input-regular" placeholder="Название"  value="{{request('name')}}">
                        </div>
                    </div>
                    <div class="col-md-2 col-sm-6">
                        <label class="input-group__title hidden-xs">&nbsp;</label>
                        <button class="btn full-width" >Применить</button>
                    </div>
                </div>
            </form>
            <br>
        </section>
    </div>

    <div class="container container-fluid">
        <div class="block">
            <h2 class="title-secondary">Атлеты</h2>

            <table class="table records">
                <colgroup>
                    <col span="1" style="width: 3%;">
                    <col span="1" style="width: 15%;">
                    <col span="1" style="width: 12%;">
                    <col span="1" style="width: 12%;">
                    <col span="1" style="width: 12%;">
                    <col span="1" style="width: 12%;">
                    <col span="1" style="width: 15%;">
                </colgroup>
                <thead>
                <tr>
                    <th>№</th>
                    <th>ФИО</th>
                    <th>Дата рождение</th>
                    <th>Весовая категория</th>
                    <th>Рейтинг</th>
                    <th>Месторождение</th>
                    <th>Федерация</th>
                    <th>Действия</th>
                </tr>
                </thead>
                <tbody>
                @forelse($athletes as $athlete)
                    <tr>
                        <td>{{$athlete->id}}</td>
                        <td>{{$athlete->fullname}}</td>
                        <td>{{$athlete->date_of_birth}}</td>
                        <td>{{$athlete->weight}}</td>
                        <td>{{$athlete->rating}}</td>
                        <td>{{$athlete->birthplace}}</td>
                        <td>{{$athlete->federation->name}}</td>
                        <td>
                            <div class="action-buttons">
                                <a href="{{ route('admin.athlete.show', [$currentFederation->site, $athlete->id]) }}" title="Посмотреть" class="icon-btn icon-btn--green icon-eye"></a>
                                <a href="{{ route('admin.athlete.edit', [$currentFederation->site, $athlete->id]) }}" title="Редактировать" class="icon-btn icon-btn--yellow icon-edit"></a>
                                <form action="{{ route('admin.athlete.delete', [$currentFederation->site, $athlete->id ] )}}" method="post">
                                    @csrf
                                    @method('delete')
                                    <input type="hidden" name="id" value="{{$athlete->id}}">
                                    <button type="submit" style="border:none;" class="icon-btn icon-btn--pink icon-delete" ></button>
                                </form>
                            </div>
                        </td>
                    </tr>
                @empty
                    <div class="text-align:center">No athletes yet.</div>
                @endforelse
                </tbody>
            </table>

            <ul class="pagination">
                <li class="previous_page"><a {{$athletes->currentPage() === 1 ? 'disabled' : '' }} href="{{ route('admin.athletes', [$currentFederation->site]) }}?page={{ $athletes->currentPage() - 1 }}"><i class="icon-left"></i></a></li>

                @for($page = 1; $page <= $athletes->lastPage(); $page++)
                    <li><a {{ $page === $athletes->currentPage() ? 'class="active"' : '' }} href="{{ route('admin.athletes', [$currentFederation->site]) }}?page={{$page}}">{{ $page }}</a></li>
                @endfor

                <li class="next_page"><a {{$athletes->currentPage() === $athletes->lastPage() ? 'disabled' : '' }} href="{{ route('admin.athletes', [$currentFederation->site]) }}?page={{ $athletes->currentPage() + 1 }}"><i class="icon-right"></i></a></li>
            </ul>
        </div>
    </div>

@extends('admin.layouts.footer')
@section('content')
    <!--Only this page's scripts-->
    <!---->
@endsection
