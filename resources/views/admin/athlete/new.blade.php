@include('admin.layouts.header')

<div class="container container-fluid">
    <ul class="breadcrumbs">
        <li><span>{{$currentFederation->name}}</span></li>
        <li><a href="{{ route('admin.athletes', [$currentFederation->site]) }}" title="Атлеты">Атлеты</a></li>
        <li><span>New</span></li>
    </ul>

    <form class="block" method="post" action="{{ route('admin.athlete.store', [$currentFederation->site]) }}" enctype="multipart/form-data">
        @csrf
        @method('post')

        <div class="input-group">
            <label class="input-group__title"> ФИО</label>
            <input type="text" name="fullname" value="{{ old('fullname')}}" placeholder="ФИО" class="input-regular" required>
        </div>
        <br>
        <div class="input-group">
            <label class="input-group__title"> Дата рождение</label>
            <input type="date" name="date_of_birth" value="{{ old('date_of_birth')}}" placeholder="Дата рождение" class="input-regular" required>
        </div>
        <br>
        <div class="input-group">
            <label class="input-group__title"> Месторождение</label>
            <input type="text" name="birthplace" value="{{ old('birthplace')}}" placeholder="Месторождение" class="input-regular" required>
        </div>
        <br>
        <div class="input-group">
            <label class="input-group__title"> Вес</label>
            <input type="number" name="weight" value="{{ old('weight')}}" placeholder="Вес" class="input-regular" required>
        </div>
        <br>
        <div class="input-group">
            <label class="input-group__title"> Рейтинг</label>
            <input type="number" name="rating" value="{{ old('rating')}}" placeholder="Рейтинг" class="input-regular" required>
        </div>
        <br>
        <div class="input-group">
            <label class="input-group__title"> Федерация</label>
            <select name="federation_id" class="chosen no-search input-regular" data-placeholder="Все" required>
                @forelse($allFederations as $federation)
                    <option value="{{$federation->id}}" {{$currentFederation->id ===  $federation->id ? 'selected' : ''}}>{{$federation->name}}</option>
                @endforeach
            </select>
        </div>
        <br>
        <div class="input-group">
            <label class="input-group__title"> About</label>
            <textarea name="about" placeholder="About" class="input-regular" required>{{ old('about')}}</textarea>
        </div>
        <br>
        <div class="input-group">
            <label class="file-input"> <span class="file-selected">Прикрепить Фото</span>
                <span class="file-input__clear icon-close"></span><span class="icon-upload"></span>
                <input type="file" style="display:none;" class="fileUpload" name="image">
            </label>
        </div>
        <br>
        <div class="buttons">
            <div>
                <button type="submit" class="btn btn--green">Сохранить</button>
            </div>
        </div>

        @if ($errors->{ $bag ?? 'default' }->any())
            <ul class="field mt-6 list-reset">
                @foreach ($errors->{ $bag ?? 'default' }->all() as $error)
                    <li class="sm:text-xs text-red">{{ $error }}</li>
                @endforeach
            </ul>
        @endif
    </form>
</div>
@extends('admin.layouts.footer')
@section('content')
    <!--Only this page's scripts-->
    <!---->
@endsection
