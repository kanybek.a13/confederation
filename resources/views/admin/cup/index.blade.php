@include('admin.layouts.header')

<main class="main">
    <div class="container ">
        <section>
            <h2 class="title-primary">Кубки</h2>
            <form action="{{ route('admin.cups', [$currentFederation->site]) }}" method="GET">
                <div class="row row--multiline">
                    <div class="col-md-3 col-sm-6">
                        <div class="input-group">
                            <label class="input-group__title">Название</label>
                            <input type="text" name="name" class="input-regular" placeholder="Название"  value="{{request('name')}}">
                        </div>
                    </div>
                    <div class="col-md-2 col-sm-6">
                        <label class="input-group__title hidden-xs">&nbsp;</label>
                        <button class="btn full-width" >Применить</button>
                    </div>
                </div>
            </form>
            <br>
        </section>
    </div>

    <div class="container container-fluid">
        <div class="block">
            <h2 class="title-secondary">Список тренеров</h2>
            <table class="table records">
                <colgroup>
                    <col span="1" style="width: 3%;">
                    <col span="1" style="width: 20%;">
                    <col span="1" style="width: 15%;">
                    <col span="1" style="width: 15%;">
                    <col span="1" style="width: 15%;">
                    <col span="1" style="width: 15%;">
                    <col span="1" style="width: 15%;">
                </colgroup>
                <thead>
                    <tr>
                        <th>№</th>
                        <th>Название</th>
                        <th>Заглавие</th>
                        <th>Действия</th>
                    </tr>
                </thead>
                <tbody>
                    @forelse($allCups as $cup)
                        <tr>
                            <td>{{$cup->id}}</td>
                            <td>{{$cup->name}}</td>
                            <td>{{$cup->title}}</td>
                            <td>
                                <div class="action-buttons">
                                    <a href="{{ route('admin.cup.show', [$currentFederation->site, $cup->id]) }}" title="Посмотреть" class="icon-btn icon-btn--green icon-eye"></a>
                                    <a href="{{ route('admin.cup.edit', [$currentFederation->site, $cup->id]) }}" title="Редактировать" class="icon-btn icon-btn--yellow icon-edit"></a>
                                    <form action="{{ route('admin.cup.delete', [$currentFederation->site, $cup->id]) }}" method="post">
                                        @csrf
                                        @method('delete')
                                        <input type="hidden" name="id" value="{{$cup->id}}">
                                        <button type="submit" style="border:none;" class="icon-btn icon-btn--pink icon-delete" ></button>
                                    </form>
                                </div>
                            </td>
                        </tr>
                    @empty
                        No cups yet!
                    @endforelse
                </tbody>
            </table>

            <ul class="pagination">
                <li class="previous_page"><a {{$allCups->currentPage() === 1 ? 'disabled' : '' }} href="{{ route('admin.cups', [$currentFederation->site]) }}?page={{ $allCups->currentPage() - 1 }}"><i class="icon-left"></i></a></li>

                @for($page = 1; $page <= $allCups->lastPage(); $page++)
                    <li><a {{ $page === $allCups->currentPage() ? 'class="active"' : '' }} href="{{ route('admin.cups', [$currentFederation->site]) }}?page={{$page}}">{{ $page }}</a></li>
                @endfor

                <li class="next_page"><a {{$allCups->currentPage() === $allCups->lastPage() ? 'disabled' : '' }} href="{{ route('admin.cups', [$currentFederation->site]) }}?page={{ $allCups->currentPage() + 1 }}"><i class="icon-right"></i></a></li>
            </ul>
        </div>
    </div>

@extends('admin.layouts.footer')
@section('content')
    <!--Only this page's scripts-->
    <!---->
@endsection
