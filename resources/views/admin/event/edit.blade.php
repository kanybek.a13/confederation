@include('admin.layouts.header')

<div class="container container-fluid">
    <ul class="breadcrumbs">
        <li><span>{{$currentFederation->name}}</span></li>
        <li><a href="{{ route('admin.events', [$currentFederation->site]) }}" title="Календарь событий">Календарь событий</a></li>
        <li><a href="{{ route('admin.event.show', [$currentFederation->site, $event->id]) }}" title="{{$event->name}}">{{$event->name}}</a></li>
        <li><span>Edit</span></li>
    </ul>
    <div class="tabs-contents">
        <div class="active">
            <form class="block" method="post" action="{{ route('admin.event.update', [$currentFederation->site, $event->id]) }}">
                @csrf
                @method('put')

                <div class="input-group">
                    <label class="input-group__title"> Название</label>
                    <input type="text" name="name" value="{{$event->name}}" placeholder="Название" class="input-regular" required>
                </div>
                <br>
                <div class="input-group">
                    <label class="input-group__title"> Дата начало</label>
                    <input type="date" name="start_date" value="{{$event->start_date}}" placeholder="Дата начало" class="input-regular" required>
                </div>
                <br>
                <div class="input-group">
                    <label class="input-group__title"> Дата окончание</label>
                    <input type="date" name="finish_date" value="{{$event->finish_date}}" placeholder="Дата окончание" class="input-regular" required>
                </div>
                <br>
                <div class="input-group">
                    <label class="input-group__title"> Место</label>
                    <input type="text" name="location" value="{{$event->location}}" placeholder="Место" class="input-regular" required>
                </div>
                <br>
                <div class="input-group">
                    <label class="input-group__title"> Федерация</label>
                    <select name="federation_id" class="chosen no-search input-regular" data-placeholder="Все" required>
                        @forelse($allFederations as $federation)
                            <option value="{{$federation->id}}" {{$currentFederation->id ===  $federation->id ? 'selected' :''}} >{{$federation->name }}</option>
                            @endforeach
                    </select>
                </div>
                <br>
                <div class="input-group">
                    <label class="input-group__title"> About</label>
                    <textarea name="about" placeholder="About" class="input-regular">{{$event->about}}</textarea>
                </div>
                <br>
                <div class="buttons">
                    <div>
                        <button type="submit" class="btn btn--green">Сохранить</button>
                    </div>
                </div>

                @if ($errors->{ $bag ?? 'default' }->any())
                    <ul class="field mt-6 list-reset">
                        @foreach ($errors->{ $bag ?? 'default' }->all() as $error)
                            <li class="sm:text-xs text-red">{{ $error }}</li>
                        @endforeach
                    </ul>
                @endif
            </form>
        </div>
    </div>
</div>

@extends('admin.layouts.footer')
@section('content')
    <!--Only this page's scripts-->
    <!---->
@endsection
