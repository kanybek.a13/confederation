@include('admin.layouts.header')

<main class="main">
    <div class="container container-fluid">
        <br>
        <ul class="breadcrumbs">
            <li><span>{{$currentFederation->name}}</span></li>
            <li><a href="{{ route('admin.events', [$currentFederation->site]) }}" title="Календарь событий">Календарь событий</a></li>
        </ul>
    </div>
    <div class="container ">
        <section>
            <h2 class="title-primary"> Календарь событий</h2>

            <form action="{{ route('admin.events', [$currentFederation->site]) }}" method="GET">
                <div class="row row--multiline">
                    <div class="col-md-3 col-sm-6">
                        <div class="input-group">
                            <label class="input-group__title">Название</label>
                            <input type="text" name="name" class="input-regular" placeholder="Название"  value="{{request('name')}}">
                        </div>
                    </div>
                    <div class="col-md-2 col-sm-6">
                        <label class="input-group__title hidden-xs">&nbsp;</label>
                        <button class="btn full-width" >Применить</button>
                    </div>
                </div>
            </form>
            <br>
        </section>
    </div>

    <div class="container container-fluid">
        <div class="block">
            <h2 class="title-secondary">Календарь событий</h2>

            <table class="table records">
                <colgroup>
                    <col span="1" style="width: 3%;">
                    <col span="1" style="width: 20%;">
                    <col span="1" style="width: 20%;">
                    <col span="1" style="width: 12%;">
                    <col span="1" style="width: 12%;">
                    <col span="1" style="width: 15%;">
                    <col span="1" style="width: 15%;">
                </colgroup>
                <thead>
                <tr>
                    <th>№</th>
                    <th>Название</th>
                    <th>Место</th>
                    <th>Дата начало</th>
                    <th>Дата окончание</th>
                    <th>Федерация</th>
                    <th>Действия</th>
                </tr>
                </thead>
                <tbody>
                @forelse($events as $event)
                    <tr>
                        <td>{{$event->id}}</td>
                        <td>{{$event->name}}</td>
                        <td>{{$event->location}}</td>
                        <td>{{$event->start_date}}</td>
                        <td>{{$event->finish_date}}</td>
                        <td>{{$event->federation->name}}</td>
                        <td>
                            <div class="action-buttons">
                                <a href="{{ route('admin.event.show', [$currentFederation->site, $event->id]) }}" title="Посмотреть" class="icon-btn icon-btn--green icon-eye"></a>
                                <a href="{{ route('admin.event.edit', [$currentFederation->site, $event->id]) }}" title="Редактировать" class="icon-btn icon-btn--yellow icon-edit"></a>
                                <form action="{{ route('admin.event.delete', [$currentFederation->site, $event->id ] )}}" method="post">
                                    @csrf
                                    @method('delete')
                                    <input type="hidden" name="id" value="{{$event->id}}">
                                    <button type="submit" style="border:none;" class="icon-btn icon-btn--pink icon-delete" ></button>
                                </form>
                            </div>
                        </td>
                    </tr>
                @empty
                    <div class="text-align:center">No documents yet.</div>
                @endforelse
                </tbody>
            </table>

            <ul class="pagination">
                <li class="previous_page"><a {{$events->currentPage() === 1 ? 'disabled' : '' }} href="{{ route('admin.athletes', [$currentFederation->site]) }}?page={{ $events->currentPage() - 1 }}"><i class="icon-left"></i></a></li>

                @for($page = 1; $page <= $events->lastPage(); $page++)
                    <li><a {{ $page === $events->currentPage() ? 'class="active"' : '' }} href="{{ route('admin.athletes', [$currentFederation->site]) }}?page={{$page}}">{{ $page }}</a></li>
                @endfor

                <li class="next_page"><a {{$events->currentPage() === $events->lastPage() ? 'disabled' : '' }} href="{{ route('admin.athletes', [$currentFederation->site]) }}?page={{ $events->currentPage() + 1 }}"><i class="icon-right"></i></a></li>
            </ul>
        </div>
    </div>

@extends('admin.layouts.footer')
@section('content')
    <!--Only this page's scripts-->
        <!---->
@endsection
