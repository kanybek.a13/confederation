@include('admin.layouts.header')

<main class="main">
    <div class="container ">
        <section>
            <h2 class="title-primary">Федерации</h2>

            <form action="/admin-panel/federations" method="GET">
                <div class="row row--multiline">
                    <div class="col-md-3 col-sm-6">
                        <div class="input-group">
                            <label class="input-group__title">Название</label>
                            <input type="text" name="name" class="input-regular" placeholder="Название"  value="{{request('name')}}">
                        </div>
                    </div>

                    <div class="col-md-2 col-sm-6">
                        <label class="input-group__title hidden-xs">&nbsp;</label>
                        <button class="btn full-width" >Применить</button>
                    </div>
                </div>
            </form>
            <br>
        </section>
    </div>

    <div class="container container-fluid">
        <div class="block">
            <h2 class="title-secondary">Список федерации</h2>

            <table class="table records">
                <colgroup>
                    <col span="1" style="width: 3%;">
                    <col span="1" style="width: 20%;">
                    <col span="1" style="width: 10%;">
                    <col span="1" style="width: 10%;">
                    <col span="1" style="width: 10%;">
                    <col span="1" style="width: 10%;">
                    <col span="1" style="width: 15%;">
                    <col span="1" style="width: 15%;">
                    <col span="1" style="width: 15%;">
                </colgroup>
                <thead>
                <tr>
                    <th>№</th>
                    <th>Название</th>
                    <th>Спорт</th>
                    <th>Телефон</th>
                    <th>Почта</th>
                    <th>Факс</th>
                    <th>Веб-сайт</th>
                    <th>Адрес</th>
                    <th>Действия</th>
                </tr>
                </thead>
                <tbody>
                    @forelse($federations as $federation)
                        <tr>
                            <td>{{$federation->id}}</td>
                            <td>{{$federation->name}}</td>
                            <td>{{$federation->sport}}</td>
                            <td>{{$federation->phone}}</td>
                            <td>{{$federation->email}}</td>
                            <td>{{$federation->fax}}</td>
                            <td>{{$federation->website}}</td>
                            <td>{{$federation->address}}</td>
                            <td>
                                <div class="action-buttons">
                                    <a href="/admin-panel/federation/{{$federation->site}}" title="Посмотреть" class="icon-btn icon-btn--green icon-eye"></a>
                                    <a href="/admin-panel/federation/{{$federation->site}}/edit" title="Редактировать" class="icon-btn icon-btn--yellow icon-edit"></a>
                                    <form action="{{ route('admin.federation.delete', $federation->id) }}" method="post">
                                        @csrf
                                        @method('delete')
                                        <input type="hidden" name="id" value="{{$federation->id}}">
                                        <button type="submit" style="border:none;" class="icon-btn icon-btn--pink icon-delete" ></button>
                                    </form>
                                </div>
                            </td>
                        </tr>
                    @empty
                        <div class="text-align:center">No federations yet.</div>
                    @endforelse
                </tbody>
            </table>

            <ul class="pagination">
                <li class="previous_page"><a {{$federations->currentPage() === 1 ? 'disabled' : '' }} href="{{request()->getRequestUri()}}?page={{ $federations->currentPage() - 1 }}"><i class="icon-left"></i></a></li>

                @for($page = 1; $page <= $federations->lastPage(); $page++)
                    <li><a {{ $page === $federations->currentPage() ? 'class="active"' : '' }} href="/admin-panel/federations?page={{$page}}">{{ $page }}</a></li>
                @endfor

                <li class="next_page"><a {{$federations->currentPage() === $federations->lastPage() ? 'disabled' : '' }} href="/users?page={{ $federations->currentPage() + 1 }}"><i class="icon-right"></i></a></li>
            </ul>
        </div>
    </div>

@extends('admin.layouts.footer')
@section('content')
    <!--Only this page's scripts-->
    <!---->
@endsection
