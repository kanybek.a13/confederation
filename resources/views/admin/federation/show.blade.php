@include('admin.layouts.header')

<div class="container container-fluid">
    <ul class="breadcrumbs">
        <li><a href="/admin-panel/federations" title="Доктор">Федерации</a></li>
        <li><span>{{$federation->sport}}</span></li>
    </ul>

    <div class="fund-header">
        <div class="fund-header__left">
            <div class="fund-header__id">#{{$federation->id}}</div>
            <h1 class="fund-header__title">{{$federation->name}}</h1>
        </div>
        <div class="fund-header__right">
            <div class="property">
                <div class="property__title">Дата создания</div>
                <div class="property__text">{{$federation->created_at}}</div>
            </div>
            <div class="property">
                <div class="property__title">Дата изменения </div>
                <div class="property__text">{{$federation->updated_at}}<br></div>
            </div>
        </div>
    </div>

    <div class="block">
        <div class="tabs">

            <div class="mobile-dropdown">
                <div class="mobile-dropdown__title dynamic">Основная информация</div>
                <div class="mobile-dropdown__desc">
                    <ul class="tabs-titles">
                        <li class="active"><a href="javascript:;" title="Основные реквизиты">Основная информация</a></li>
                    </ul>
                    <div class="input-group">
                        <div class="row row--multiline">
                            <div class="col-md-4 col-sm-6">
                                <a href="/admin-panel/federation/{{$federation->site}}/edit" title="Редактировать" class="icon-btn icon-btn--yellow icon-edit" align="right" ></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="tabs-contents">
                <div class="active">
                    <div class="col-sm-4">
                        <label class="input-group__title"> Логотип</label>
                        <img class="article-image" src="{{$federation->logo}}" width="100" height="100">
                    </div>
                    <br>
                    <div class="col-sm-4">
                        <label class="input-group__title"> Фото</label>
                        <img class="article-image" src="{{$federation->image}}" width="200" height="200">
                    </div>
                    <br>
                    <div class="input-group">
                        <label class="input-group__title"> Название</label>
                        <input type="text" name="name" value="{{$federation->name}}" placeholder="Название" class="input-regular" disabled>
                    </div>
                    <br>
                    <div class="input-group">
                        <label class="input-group__title"> Название спорта</label>
                        <input type="text" name="sport" value="{{$federation->sport}}" placeholder="Название спорта" class="input-regular" disabled>
                    </div>
                    <br>
                    <div class="input-group">
                        <label class="input-group__title"> Номер телефона</label>
                        <input type="text" name="phone" value="{{$federation->phone}}" placeholder="Номер телефона" class="input-regular" disabled>
                    </div>
                    <br>
                    <div class="input-group">
                        <label class="input-group__title"> Почта</label>
                        <input type="email" name="email" value="{{$federation->email}}" placeholder="Почта" class="input-regular" disabled>
                    </div>
                    <br>
                    <div class="input-group">
                        <label class="input-group__title"> Факс</label>
                        <input type="text" name="fax" value="{{$federation->fax}}" placeholder="Факс" class="input-regular" disabled>
                    </div>
                    <br>
                    <div class="input-group">
                        <label class="input-group__title"> Веб-сайт</label>
                        <input type="text" name="website" value="{{$federation->website}}" placeholder="Веб-сайт" class="input-regular" disabled>
                    </div>
                    <br>
                    <div class="input-group">
                        <label class="input-group__title"> Адрес</label>
                        <input type="text" name="role" value="{{$federation->address}}" placeholder="Адрес" class="input-regular" disabled>
                    </div>
                </div>
            </div>
        </div>
    </div>
@extends('admin.layouts.footer')
@section('content')
    <!--Only this page's scripts-->
    <!---->
@endsection
