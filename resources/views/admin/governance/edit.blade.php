@include('admin.layouts.header')

<div class="container container-fluid">
    <ul class="breadcrumbs">
        <li><span>{{$currentFederation->name}}</span></li>
            <li><a href="{{ route('admin.governance', [$currentFederation->site]) }}" title="Руководство">Руководство</a></li>
        <li><a href="{{ route('admin.governance.show', [$currentFederation->site, $governance->id]) }}" title="{{$governance->fullname}}">{{$governance->fullname}}</a></li>
        <li><span>Edit</span></li>
    </ul>
    <div class="tabs-contents">
        <div class="active">
            <form class="block" method="post" action="{{ route('admin.governance.update', [$currentFederation->site, $governance->id]) }}">
                @csrf
                @method('put')

                <div class="input-group">
                    <label class="input-group__title"> ФИО</label>
                    <input type="text" name="fullname" value="{{$governance->fullname}}" placeholder="ФИО" class="input-regular">
                </div>
                <br>
                <div class="input-group">
                    <label class="input-group__title"> Должность</label>
                    <input type="text" name="position" value="{{$governance->position}}" placeholder="Должность" class="input-regular">
                </div>
                <br>
                <div class="input-group">
                    <label class="input-group__title"> Федерация</label>
                    <select name="federation_id" class="chosen no-search input-regular" data-placeholder="Все" required>
                        @forelse($allFederations as $federation)
                            <option value="{{$federation->id}}" {{$currentFederation->id ===  $federation->id ? 'selected' :''}} >{{$federation->name }}</option>
                            @endforeach
                    </select>
                </div>
                <br>
                <div class="buttons">
                    <div>
                        <button type="submit" class="btn btn--green">Сохранить</button>
                    </div>
                </div>

                @if ($errors->{ $bag ?? 'default' }->any())
                    <ul class="field mt-6 list-reset">
                        @foreach ($errors->{ $bag ?? 'default' }->all() as $error)
                            <li class="sm:text-xs text-red">{{ $error }}</li>
                        @endforeach
                    </ul>
                @endif
            </form>
        </div>
    </div>
</div>

@extends('admin.layouts.footer')
@section('content')
    <!--Only this page's scripts-->
    <!---->
@endsection
