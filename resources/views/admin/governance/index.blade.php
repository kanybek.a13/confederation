@include('admin.layouts.header')

<main class="main">
    <div class="container ">
        <section>
            <h2 class="title-primary">Руководство</h2>
            <form action="{{ route('admin.governance', [$currentFederation->site]) }}" method="GET">
                <div class="row row--multiline">
                    <div class="col-md-3 col-sm-6">
                        <div class="input-group">
                            <label class="input-group__title">ФИО</label>
                            <input type="text" name="fullname" class="input-regular" placeholder="ФИО"  value="{{request('fullname')}}">
                        </div>
                    </div>
                    <div class="col-md-2 col-sm-6">
                        <label class="input-group__title hidden-xs">&nbsp;</label>
                        <button class="btn full-width" >Применить</button>
                    </div>
                </div>
            </form>
            <br>
        </section>
    </div>

    <div class="container container-fluid">
        <div class="block">
            <h2 class="title-secondary">Список руководителей</h2>
            <table class="table records">
                <colgroup>
                    <col span="1" style="width: 3%;">
                    <col span="1" style="width: 25%;">
                    <col span="1" style="width: 30%;">
                    <col span="1" style="width: 15%;">
                    <col span="1" style="width: 15%;">
                    <col span="1" style="width: 15%;">
                    <col span="1" style="width: 15%;">
                </colgroup>
                <thead>
                    <tr>
                        <th>№</th>
                        <th>ФИО</th>
                        <th>Должность</th>
                        <th>Федерация</th>
                        <th>Действия</th>
                    </tr>
                </thead>
                <tbody>
                    @forelse($governances as $governance)
                        <tr>
                            <td>{{$governance->id}}</td>
                            <td>{{$governance->fullname}}</td>
                            <td>{{$governance->position}}</td>
                            <td>{{$governance->federation->name}}</td>
                            <td>
                                <div class="action-buttons">
                                    <a href="{{ route('admin.governance.show', [$currentFederation->site, $governance->id]) }}" title="Посмотреть" class="icon-btn icon-btn--green icon-eye"></a>
                                    <a href="{{ route('admin.governance.edit', [$currentFederation->site, $governance->id]) }}" title="Редактировать" class="icon-btn icon-btn--yellow icon-edit"></a>
                                    <form action="{{ route('admin.governance.delete', [$currentFederation->site, $governance->id]) }}" method="post">
                                        @csrf
                                        @method('delete')
                                        <input type="hidden" name="id" value="{{$governance->id}}">
                                        <button type="submit" style="border:none;" class="icon-btn icon-btn--pink icon-delete" ></button>
                                    </form>
                                </div>
                            </td>
                        </tr>
                    @empty
                        No governance yet!
                    @endforelse
                </tbody>
            </table>

            <ul class="pagination">
                <li class="previous_page"><a {{$governances->currentPage() === 1 ? 'disabled' : '' }} href="{{ route('admin.coaches', [$currentFederation->site]) }}?page={{ $governances->currentPage() - 1 }}"><i class="icon-left"></i></a></li>

                @for($page = 1; $page <= $governances->lastPage(); $page++)
                    <li><a {{ $page === $governances->currentPage() ? 'class="active"' : '' }} href="{{ route('admin.coaches', [$currentFederation->site]) }}?page={{$page}}">{{ $page }}</a></li>
                @endfor

                <li class="next_page"><a {{$governances->currentPage() === $governances->lastPage() ? 'disabled' : '' }} href="{{ route('admin.coaches', [$currentFederation->site]) }}?page={{ $governances->currentPage() + 1 }}"><i class="icon-right"></i></a></li>
            </ul>
        </div>
    </div>

@extends('admin.layouts.footer')
@section('content')
    <!--Only this page's scripts-->
    <!---->
@endsection
