@include('admin.layouts.header')

<div class="container container-fluid">
    <ul class="breadcrumbs">
        <li><span>{{$currentFederation->name}}</span></li>
        <li><a href="{{ route('admin.governance', [$currentFederation->site]) }}" title="Руководство">Руководство</a></li>
        <li><span>{{$governance->fullname}}</span></li>
    </ul>

    <div class="fund-header">
        <div class="fund-header__left">
            <div class="fund-header__id">#{{$governance->id}}</div>
            <h1 class="fund-header__title">{{$governance->fullname}}</h1>
        </div>
        <div class="fund-header__right">
            <div class="property">
                <div class="property__title">Дата создания</div>
                <div class="property__text">{{$governance->created_at}}</div>
            </div>
            <div class="property">
                <div class="property__title">Дата изменения </div>
                <div class="property__text">{{$governance->updated_at}}<br></div>
            </div>
        </div>
    </div>

    <div class="block">
        <div class="tabs">
            <div class="mobile-dropdown">
                <div class="mobile-dropdown__title dynamic">Основная информация</div>
                <div class="mobile-dropdown__desc">
                    <ul class="tabs-titles">
                        <li class="active"><a href="javascript:;" title="Основные реквизиты">Основная информация</a></li>
                    </ul>
                    <div class="input-group">
                        <div class="row row--multiline">
                            <div class="col-md-4 col-sm-6">
                                <a href="{{ route('admin.governance.edit', [$currentFederation->site, $governance->id]) }}" title="Редактировать" class="icon-btn icon-btn--yellow icon-edit" align="right" ></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="tabs-contents">
                <div class="active">
                    <div class="col-sm-4">
                        <img class="article-image" src="{{$governance->image}}" width="200" height="200">
                    </div>
                    <br>
                    <div class="input-group">
                        <label class="input-group__title"> ФИО</label>
                        <input type="text" name="fullname" value="{{$governance->fullname}}" placeholder="ФИО" class="input-regular" disabled>
                    </div>
                    <br>
                    <div class="input-group">
                        <label class="input-group__title"> Должность</label>
                        <input type="text" name="position" value="{{$governance->position}}" placeholder="Должность" class="input-regular" disabled>
                    </div>
                    <br>
                    <div class="input-group">
                        <label class="input-group__title"> Федерация</label>
                        <input type="text" name="federation_id" value="{{$governance->federation->name}}" placeholder="Федерация" class="input-regular" disabled>
                    </div>
                </div>
            </div>
        </div>
    </div>

@extends('admin.layouts.footer')
@section('content')
    <!--Only this page's scripts-->
        <!---->
@endsection
