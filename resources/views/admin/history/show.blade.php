@include('admin.layouts.header')

<div class="container container-fluid">
    <ul class="breadcrumbs">
        <li><span>{{$currentFederation->name}}</span></li>
        <li><span>История</span></li>
    </ul>

    <div class="fund-header">
        <div class="fund-header__left">
            <div class="fund-header__id">#{{@$history->id}}</div>
            <h1 class="fund-header__title">{{@$history->title}}</h1>
        </div>
        <div class="fund-header__right">
            <div class="property">
                <div class="property__title">Дата создания</div>
                <div class="property__text">{{@$history->created_at}}</div>
            </div>
            <div class="property">
                <div class="property__title">Дата изменения </div>
                <div class="property__text">{{@$history->updated_at}}<br></div>
            </div>
        </div>
    </div>

    <div class="block">
        <div class="tabs">
            @if($history)
                <div class="mobile-dropdown">
                    <div class="mobile-dropdown__title dynamic">Основная информация</div>
                    <div class="mobile-dropdown__desc">
                        <ul class="tabs-titles">
                            <li class="active"><a href="javascript:;" title="Основные реквизиты">Основная информация</a></li>
                        </ul>
                        <div class="input-group">
                            <div class="row row--multiline">
                                <div class="action-buttons">
                                    <a href="{{ route('admin.history.edit', [$currentFederation->site]) }}" title="Редактировать" class="icon-btn icon-btn--yellow icon-edit"></a>
                                    <form action="{{ route('admin.history.delete', $currentFederation->site) }}" method="post">
                                        @csrf
                                        @method('delete')
                                        <button type="submit" style="border:none;" class="icon-btn icon-btn--pink icon-delete" ></button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="white-content">
                    <div class="banner">
                        <img src="{{$history->image}}">
                    </div>
                    <div class="plain-text">
                        <h4>{{$history->title}}</h4>
                        <p>{{$history->body}}</p>
                    </div>
                </div>
            @else
                No history yet!
                <a href="{{ route('admin.history.create', [$currentFederation->site]) }}">Add history</a>
            @endif
        </div>
    </div>
</div>


@extends('admin.layouts.footer')
@section('content')
    <!--Only this page's scripts-->
        <!---->
@endsection
