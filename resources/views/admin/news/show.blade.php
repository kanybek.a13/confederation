@include('admin.layouts.header')

<div class="container container-fluid">
    <ul class="breadcrumbs">
        <li><a href="/admin-panel/news" title="Новости">Новости</a></li>
        <li><span>{{$news->sport}}</span></li>
    </ul>

    <div class="fund-header">
        <div class="fund-header__left">
            <div class="fund-header__id">#{{$news->id}}</div>
            <h1 class="fund-header__title">{{$news->title}}</h1>
        </div>
        <div class="fund-header__right">
            <div class="property">
                <div class="property__title">Дата создания</div>
                <div class="property__text">{{$news->created_at}}</div>
            </div>
            <div class="property">
                <div class="property__title">Дата изменения </div>
                <div class="property__text">{{$news->updated_at}}<br></div>
            </div>
        </div>
    </div>

    <div class="block">
        <div class="tabs">

            <div class="mobile-dropdown">
                <div class="mobile-dropdown__title dynamic">Основная информация</div>
                <div class="mobile-dropdown__desc">
                    <ul class="tabs-titles">
                        <li class="active"><a href="javascript:;" title="Основные реквизиты">Основная информация</a></li>
                    </ul>
                    <div class="input-group">
                        <div class="row row--multiline">
                            <div class="col-md-4 col-sm-6">
                                <a href="/admin-panel/{{$currentFederation->site}}/news/{{$news->id}}/edit" title="Редактировать" class="icon-btn icon-btn--yellow icon-edit" align="right" ></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="tabs-contents">
                <div class="active">
                    @forelse($news->files as $file)
                        <div class="col-sm-4">
                            <img class="article-image" src="{{$file->path}}" width="200" height="200">
                        </div>
                        <br>
                    @endforeach
                    <div class="input-group">
                        <label class="input-group__title"> Название</label>
                        <input type="text" name="title" value="{{$news->title}}" placeholder="Название" class="input-regular" disabled>
                    </div>
                    <br>
                    <div class="input-group">
                        <label class="input-group__title"> Текст новости</label>
                        <textarea name="body" placeholder="Текст новости" class="input-regular" disabled>{{$news->body}}</textarea>
                    </div>
                </div>
            </div>
        </div>
    </div>
@extends('admin.layouts.footer')
@section('content')
    <!--Only this page's scripts-->
    <!---->
@endsection
