@include('admin.layouts.header')

<div class="container container-fluid">
    <ul class="breadcrumbs">
        <li><span>{{$currentFederation->name}}</span></li>
        <li><a href="{{ route('admin.results', [$currentFederation->site]) }}" title="Результаты">Результаты</a></li>
        <li><a href="{{ route('admin.result.show', [$currentFederation->site, $result->id]) }}" title="{{$result->fullname}}">{{$result->fullname}}</a></li>
        <li><span>Edit</span></li>
    </ul>
    <div class="tabs-contents">
        <div class="active">
            <form class="block" method="post" action="{{ route('admin.result.update', [$currentFederation->site, $result->id]) }}">
                @csrf
                @method('put')

                <div class="input-group">
                    <label class="input-group__title"> Название</label>
                    <input type="text" name="name" value="{{$result->name}}" placeholder="Название" class="input-regular">
                </div>
                <br>
                <div class="buttons">
                    <div>
                        <button type="submit" class="btn btn--green">Сохранить</button>
                    </div>
                </div>

                @if ($errors->{ $bag ?? 'default' }->any())
                    <ul class="field mt-6 list-reset">
                        @foreach ($errors->{ $bag ?? 'default' }->all() as $error)
                            <li class="sm:text-xs text-red">{{ $error }}</li>
                        @endforeach
                    </ul>
                @endif
            </form>
        </div>
    </div>
</div>

@extends('admin.layouts.footer')
@section('content')
    <!--Only this page's scripts-->
    <!---->
@endsection
