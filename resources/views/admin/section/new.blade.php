@include('admin.layouts.header')

<div class="container container-fluid">
    <br>
    <ul class="breadcrumbs">
        <li><span>{{$currentFederation->name}}</span></li>
        <li><a href="{{ route('admin.sections', [$currentFederation->site]) }}" title="Секции">Секции</a></li>
        <li><span>Новый</span></li>
    </ul>
    <form class="block" method="post" action="{{ route('admin.section.store', [$currentFederation->site]) }}" enctype="multipart/form-data">
        @csrf
        @method('post')

        <div class="tabs-contents">
            <div class="active">
                <div class="input-group">
                    <label class="input-group__title"> Название</label>
                    <input type="text" name="name" value="{{old('name')}}" placeholder="Название" class="input-regular" required>
                </div>
                <br>
                <div class="input-group">
                    <label class="input-group__title"> Телефон</label>
                    <input type="text" name="phone" value="{{old('phone')}}" placeholder="Телефон" class="input-regular" data-validate="phone" onfocus="$(this).inputmask('+7 999 999 99 99')" required>
                </div>
                <br>
                <div class="input-group">
                    <label class="input-group__title">Город</label>
                    <select name="city_id" class="chosen no-search input-regular" data-placeholder="" required>
                        <option value="" selected disabled>Выберите город</option>

                        @forelse($cities as $city)
                            <option value="{{$city->id}}" {{old('city_id')===$city->id ? 'selected' : ''}}>{{$city->name }}</option>
                        @endforeach
                    </select>
                </div>
                <br>
                <div class="input-group">
                    <label class="input-group__title"> Адрес</label>
                    <input type="text" name="address" value="{{old('address')}}" placeholder="Адрес" class="input-regular" required>
                </div>
                <br>
                <div class="input-group">
                    <label class="input-group__title">Федерация</label>
                    <select name="federation_id" class="chosen no-search input-regular" data-placeholder="" required>
                        <option value="" disabled>Выберите федерацию</option>

                        @forelse($allFederations as $federation)
                            <option value="{{$federation->id}}" {{ $currentFederation->id===$federation->id ? 'selected' : '' }}>{{$federation->name }}</option>
                        @endforeach
                    </select>
                </div>
                <br>
                <div class="input-group">
                    <label class="file-input"> <span class="file-selected">Прикрепить Фото</span>
                        <span class="file-input__clear icon-close"></span><span class="icon-upload"></span>
                        <input type="file" style="display:none;" class="fileUpload" name="image" required>
                    </label>
                </div>
            </div>
        </div>
        <hr>
        <div class="buttons">
            <div>
                <button type="submit" class="btn btn--green">Сохранить</button>
            </div>
        </div>

        @if ($errors->{ $bag ?? 'default' }->any())
            <ul class="field mt-6 list-reset">
                @foreach ($errors->{ $bag ?? 'default' }->all() as $error)
                    <li class="sm:text-xs text-red">{{ $error }}</li>
                @endforeach
            </ul>
        @endif
    </form>
</div>
@extends('admin.layouts.footer')
@section('content')
    <!--Only this page's scripts-->
    <!---->
@endsection
