@include('admin.layouts.header')

<div class="container container-fluid">
    <ul class="breadcrumbs">
        <li><span>{{$currentFederation->name}}</span></li>
        <li><a href="{{ route('admin.sections', [$currentFederation->site]) }}" title="Секции">Секции</a></li>
        <li><span>{{$section->name}}</span></li>
    </ul>

    <div class="fund-header">
        <div class="fund-header__left">
            <div class="fund-header__id">#{{$section->id}}</div>
            <h1 class="fund-header__title">{{$section->name}}</h1>
        </div>
        <div class="fund-header__right">
            <div class="property">
                <div class="property__title">Дата создания</div>
                <div class="property__text">{{$section->created_at}}</div>
            </div>
            <div class="property">
                <div class="property__title">Дата изменения </div>
                <div class="property__text">{{$section->updated_at}}<br></div>
            </div>
        </div>
    </div>

    <div class="block">
        <div class="tabs">
            <div class="mobile-dropdown">
                <div class="mobile-dropdown__title dynamic">Основная информация</div>
                <div class="mobile-dropdown__desc">
                    <ul class="tabs-titles">
                        <li class="active"><a href="javascript:;" title="Основные реквизиты">Основная информация</a></li>
                    </ul>
                    <div class="input-group">
                        <div class="row row--multiline">
                            <div class="col-md-4 col-sm-6">
                                <a href="{{ route('admin.section.edit', [$currentFederation->site, $section->id]) }}" title="Редактировать" class="icon-btn icon-btn--yellow icon-edit" align="right" ></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="tabs-contents">
                <div class="active">
                    <div class="col-sm-4">
                        <img class="article-image" src="{{$section->image}}" width="200" height="200">
                    </div>
                    <br>
                    <div class="input-group">
                        <label class="input-group__title"> Название</label>
                        <input type="text" name="name" value="{{$section->name}}" placeholder="Название" class="input-regular" disabled>
                    </div>
                    <br>
                    <div class="input-group">
                        <label class="input-group__title"> Телефон</label>
                        <input type="text" name="phone" value="{{$section->phone}}" placeholder="Месторождение" class="input-regular" disabled>
                    </div>
                    <br>
                    <div class="input-group">
                        <label class="input-group__title"> Город</label>
                        <input type="text" name="sity_id" value="{{$section->city->name}}" placeholder="Город" class="input-regular" disabled>
                    </div>
                    <br>
                    <div class="input-group">
                        <label class="input-group__title"> Адрес</label>
                        <input type="text" name="address" value="{{$section->address}}" placeholder="Адрес" class="input-regular" disabled>
                    </div>
                    <br>
                    <div class="input-group">
                        <label class="input-group__title"> Федерация</label>
                        <input type="text" name="federation_id" value="{{$section->federation->name}}" placeholder="Федерация" class="input-regular" disabled>
                    </div>
                </div>
            </div>
        </div>
    </div>

@extends('admin.layouts.footer')
@section('content')
    <!--Only this page's scripts-->
        <!---->
@endsection
