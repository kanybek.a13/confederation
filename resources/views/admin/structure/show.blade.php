@include('admin.layouts.header')

<div class="container container-fluid">
    <ul class="breadcrumbs">
        <li><span>{{$currentFederation->name}}</span></li>
        <li><span>Структура</span></li>
    </ul>

    <div class="fund-header">
        <div class="fund-header__left">
            <div class="fund-header__id">#{{@$structure->id}}</div>
            <h1 class="fund-header__title">{{@$structure->title}}</h1>
        </div>
        <div class="fund-header__right">
            <div class="property">
                <div class="property__title">Дата создания</div>
                <div class="property__text">{{@$structure->created_at}}</div>
            </div>
            <div class="property">
                <div class="property__title">Дата изменения </div>
                <div class="property__text">{{@$structure->updated_at}}<br></div>
            </div>
        </div>
    </div>

    <div class="block">
        <div class="tabs">
            @if($structure)
                <div class="mobile-dropdown">
                    <div class="mobile-dropdown__title dynamic">Основная информация</div>
                    <div class="mobile-dropdown__desc">
                        <ul class="tabs-titles">
                            <li class="active"><a href="javascript:;" title="Основные реквизиты">Основная информация</a></li>
                        </ul>
                        <div class="input-group">
                            <div class="row row--multiline">
                                <div class="action-buttons">
                                    <a href="{{ route('admin.structure.edit', [$currentFederation->site]) }}" title="Редактировать" class="icon-btn icon-btn--yellow icon-edit"></a>
                                    <form action="{{ route('admin.structure.delete', $currentFederation->site) }}" method="post">
                                        @csrf
                                        @method('delete')
                                        <button type="submit" style="border:none;" class="icon-btn icon-btn--pink icon-delete" ></button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="white-content">
                    <div class="banner">
                        <img src="{{$structure->image}}">
                    </div>
                    <div class="plain-text">
                        <h4>{{$structure->title}}</h4>
                        <p>{{$structure->body}}</p>
                    </div>
                </div>
            @else
                No structure yet!
                <a href="{{ route('admin.structure.create', [$currentFederation->site]) }}">Add structure</a>
            @endif
        </div>
    </div>
</div>


@extends('admin.layouts.footer')
@section('content')
    <!--Only this page's scripts-->
        <!---->
@endsection
