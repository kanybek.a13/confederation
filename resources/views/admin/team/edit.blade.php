@include('admin.layouts.header')

<div class="container container-fluid">
    <br>
    <ul class="breadcrumbs">
        <li><span>{{$currentFederation->name}}</span></li>
        <li><a href="{{ route('admin.teams', [$currentFederation->site]) }}" title="Команды">Команды</a></li>
        <li><a href="{{ route('admin.team.show', [$currentFederation->site, $team->id]) }}" title="{{$team->name}}">{{$team->name}}</a></li>
        <li><span>Edit</span></li>
    </ul>
    <div class="tabs-contents">
        <div class="active">
            <form class="block" method="post" action="{{route('admin.team.update', [$currentFederation->site, $team->id])}}">
                @csrf
                @method('put')

                <div class="input-group">
                    <label class="input-group__title"> Название</label>
                    <input type="text" name="name" value="{{$team->name}}" placeholder="Название" class="input-regular" required>
                </div>
                <br>
                <div class="input-group">
                    <label class="input-group__title">Федерация</label>

                    <select name="federation_id" class="chosen no-search input-regular" data-placeholder="" required>
                        @forelse($allFederations as $federation)
                            <option value="{{$federation->id}}" {{ $team->federation->id ===  $federation->id ? 'selected' :''}} >{{$federation->name }}</option>
                        @endforeach
                    </select>
                </div>
                <br>
                <div class="buttons">
                    <div>
                        <button type="submit" class="btn btn--green">Сохранить</button>
                    </div>
                </div>

                @if ($errors->{ $bag ?? 'default' }->any())
                    <ul class="field mt-6 list-reset">
                        @foreach ($errors->{ $bag ?? 'default' }->all() as $error)
                            <li class="sm:text-xs text-red">{{ $error }}</li>
                        @endforeach
                    </ul>
                @endif
            </form>
        </div>
    </div>
</div>

@extends('admin.layouts.footer')
@section('content')
    <!--Only this page's scripts-->
    <!---->
@endsection
