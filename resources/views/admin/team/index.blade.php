@include('admin.layouts.header')

<main class="main">
    <div class="container ">
        <section>
            <h2 class="title-primary">Сборные команды</h2>

            <form action="{{ route('admin.teams', [$currentFederation->site]) }}" method="GET">
                <div class="row row--multiline">
                    <div class="col-md-3 col-sm-6">
                        <div class="input-group">
                            <label class="input-group__title">Название</label>
                            <input type="text" name="name" class="input-regular" placeholder="Название" value="{{request('name')}}">
                        </div>
                    </div>

                    <div class="col-md-2 col-sm-6">
                        <label class="input-group__title hidden-xs">&nbsp;</label>
                        <button class="btn full-width" >Применить</button>
                    </div>
                </div>
            </form>
            <br>
        </section>
    </div>

    <div class="container container-fluid">
        <div class="block">
            <h2 class="title-secondary">Список команд</h2>

            <table class="table records">
                <colgroup>
                    <col span="1" style="width: 3%;">
                    <col span="1" style="width: 20%;">
                    <col span="1" style="width: 20%;">
                    <col span="1" style="width: 15%;">
                    <col span="1" style="width: 15%;">
                    <col span="1" style="width: 15%;">
                    <col span="1" style="width: 15%;">
                </colgroup>
                <thead>
                <tr>
                    <th>№</th>
                    <th>Название</th>
                    <th>Федерация</th>
                    <th>Действия</th>
                </tr>
                </thead>
                <tbody>
                    @forelse($allTeams as $team)
                        <tr>
                            <td>{{$team->id}}</td>
                            <td>{{$team->name}}</td>
                            <td>{{$team->federation->name}}</td>
                            <td>
                                <div class="action-buttons">
                                    <a href="{{ route('admin.team.show', [$currentFederation->site, $team->id]) }}" title="Посмотреть" class="icon-btn icon-btn--green icon-eye"></a>
                                    <a href="{{ route('admin.team.edit', [$currentFederation->site, $team->id]) }}" title="Редактировать" class="icon-btn icon-btn--yellow icon-edit"></a>
                                    <form action="{{ route('admin.team.delete', [$currentFederation->site, $team->id]) }}" method="post">
                                        @csrf
                                        @method('delete')
                                        <input type="hidden" name="id" value="{{$team->id}}">
                                        <button type="submit" style="border:none;" class="icon-btn icon-btn--pink icon-delete" ></button>
                                    </form>
                                </div>
                            </td>
                        </tr>
                    @empty
                        <div class="text-align:center">No teams yet.</div>
                    @endforelse
                </tbody>
            </table>

            <ul class="pagination">
                <li class="previous_page"><a {{$allTeams->currentPage() === 1 ? 'disabled' : '' }} href="{{ route('admin.teams', [$currentFederation->site]) }}?page={{ $allTeams->currentPage() - 1 }}"><i class="icon-left"></i></a></li>

                @for($page = 1; $page <= $allTeams->lastPage(); $page++)
                    <li><a {{ $page === $allTeams->currentPage() ? 'class="active"' : '' }} href="{{ route('admin.teams', [$currentFederation->site]) }}?page={{$page}}">{{ $page }}</a></li>
                @endfor

                <li class="next_page"><a {{$allTeams->currentPage() === $allTeams->lastPage() ? 'disabled' : '' }} href="{{ route('admin.teams', [$currentFederation->site]) }}?page={{ $allTeams->currentPage() + 1 }}"><i class="icon-right"></i></a></li>
            </ul>
        </div>
    </div>

@extends('admin.layouts.footer')
@section('content')
    <!--Only this page's scripts-->
    <!---->
@endsection
