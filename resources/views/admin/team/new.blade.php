@include('admin.layouts.header')

<div class="container container-fluid">
    <br>
    <ul class="breadcrumbs">
        <li><span>{{$currentFederation->name}}</span></li>
        <li><a href="{{ route('admin.teams', [$currentFederation->site]) }}" title="Команды">Команды</a></li>
        <li><span>Новый</span></li>
    </ul>
    <form class="block" method="post" action="{{ route('admin.team.store', [$currentFederation->site]) }}">
        @csrf
        @method('post')

        <div class="tabs-contents">
            <div class="active">
                <div class="input-group">
                    <label class="input-group__title"> Название</label>
                    <input type="text" name="name" value="" placeholder="Название" class="input-regular" required>
                </div>
                <br>
                <div class="input-group">
                    <label class="input-group__title">Федерация</label>
                    <select name="federation_id" class="chosen no-search input-regular" data-placeholder="" required>
                        <option value="" disabled>Выберите федерацию</option>

                        @forelse($allFederations as $federation)
                            <option value="{{$federation->id}}" {{ $currentFederation->id===$federation->id ? 'selected' : '' }}>{{$federation->name }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
        </div>
        <hr>
        <div class="buttons">
            <div>
                <button type="submit" class="btn btn--green">Сохранить</button>
            </div>
        </div>

        @if ($errors->{ $bag ?? 'default' }->any())
            <ul class="field mt-6 list-reset">
                @foreach ($errors->{ $bag ?? 'default' }->all() as $error)
                    <li class="sm:text-xs text-red">{{ $error }}</li>
                @endforeach
            </ul>
        @endif
    </form>
</div>
@extends('admin.layouts.footer')
@section('content')
    <!--Only this page's scripts-->
    <!---->
@endsection
