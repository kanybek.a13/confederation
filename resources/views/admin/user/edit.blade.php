@include('admin.layouts.header')

<div class="container container-fluid">
                <br>
                <ul class="breadcrumbs">
                    <li><a href="/admin/users" title="Доктор">Пользователи</a></li>
                    <li><span>{{$user->name}}</span></li>
                </ul>

                <form class="block" method="post" action="{{route('admin.user.update', $user->id)}}">
                    @csrf
                    @method('put')

                    <div class="tabs-contents">
                        <div class="active">
                            <div class="input-group">
                                <label class="input-group__title"> Имя</label>
                                <input type="text" name="name" value="{{$user->name}}" placeholder="Имя" class="input-regular">
                            </div>
                            <br>
                            <div class="input-group">
                                <label class="input-group__title"> Почта</label>
                                <input type="text" name="email" class="input-regular" value="{{$user->email}}" data-validate="email" placeholder="E-mail" required>
                            </div>
                            <br>
                            <div class="input-group">
                                <label class="input-group__title"> Пароль</label>
                                <input type="password" name="password" class="input-regular" placeholder="Введите пароль" data-validate="password" required>
                            </div>
                            <br>
                            <div class="input-group">
                                <label class="input-group__title"> Подтвердите Пароль</label>
                                <input type="password" name="password_confirmation" class="input-regular" placeholder="Подтвердите пароль" data-validate="password_confirmation" required>
                            </div>
                            <br>
                            <div class="input-group">
                                <label class="input-group__title"> Role</label>
                                <select name="role" class="input-regular chosen" data-placeholder="Role">
                                    <option value=""></option>
                                    @foreach($roles as $role)
                                        <option value="{{$role->name}}" {{$role->name === $user->roles[0]->name ? 'selected' : ''}}>
                                            {{$role->name}}
                                        </option>
                                    @endforeach
                                </select>
                            </div>
                            <br>
                        </div>
                    </div>

                    <hr>
                    <div class="buttons">
                        <div>
                            <button type="submit" class="btn btn--green">Сохранить</button>
                        </div>
                        <div>
                            <button class="btn btn--red"><a href="/admin/user/delete/{{$user->id}}" title="Удалить"></a>Удалить</button>
                        </div>
                    </div>
                </form>
            </div>

@extends('admin.layouts.footer')
@section('content')
    <!--Only this page's scripts-->
    <!---->
@endsection
