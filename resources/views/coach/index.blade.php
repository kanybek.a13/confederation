@extends('layouts.app')
@section('main')

<section class="interview">
    <div class="container">
        <div class="white-content">
            <div class="title-breadcrumbs">
                <h2>Тренерский состав</h2>
                <ul class="breadcrumbs">
                    <li><a href="/{{$currentFederation->site}}">Главная</a></li>
                    <li><span>Тренерский состав</span></li>
                </ul>
            </div>
            <div class="links">
                @forelse($teams as $team)
                    <a href="/{{$currentFederation->site}}/coaches?team_id={{$team->id}}" class="{{$team->id === request('team') ? 'active' : ''}}">{{$team->name}}</a>
                @empty
                    No teams yet!
                @endforelse
            </div>
            <div class="row">
            </div>
            <br>
            <div class="row">
                @forelse($coaches as $coach)
                    <div class="col-lg-6">
                        <a href="/{{$currentFederation->site}}/coach/{{$coach->id}}" class="d-flex align-items-center">
                            <img src="{{ $coach->image  }}">
                            <div>
                                <h3>{{ $coach->fullname  }}</h3>
                                <p class="grey">{{ $coach->position  }}</p>
                            </div>
                        </a>
                    </div>
                @empty
                    No coaches yet!
                @endforelse
            </div>
        </div>
    </div>
</section>


@endsection

