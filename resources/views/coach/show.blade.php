@extends('layouts.app')
@section('main')

    <section class="news">
        <div class="container">
            <div class="white-content">
                <div class="title-breadcrumbs">
                    <h2>{{$coach->fullname}}</h2>
                    <ul class="breadcrumbs">
                        <li><a href="/{{$currentFederation->site}}">Главная</a></li>
                        <li><a href="/{{$currentFederation->site}}/coaches">Тренерский состав</a></li>
                        <li><span>{{$coach->fullname}}</span></li>
                    </ul>
                </div>

                <div class="news__main">
                    <div class="item d-flex">
                        <div class="image">
                            <img src="{{$coach->image}}">
                        </div>
                        <div class="desc">
                            <div class="annotation">
                                <p class="grey">{{$coach->fullname}} </p>
                                <p class="grey">Должность: {{$coach->position}} </p>
                                <p class="grey">Команда :
                                    @forelse($coach->teams as $team)
                                        {{$team->name}}<br>
                                    @empty
                                        No teams yet!
                                    @endforelse
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection
