@extends('layouts.app')
@section('main')

<section class="news competitions">
    <div class="container">
        <div class="white-content">
            <h2>{{$cup->title}}</h2>
            <div class="news__main">
                <div class="item d-flex">
                    <div class="image">
                        <div class="image__item">
                            <img src="{{$cup->image_about}}">
                        </div>
                    </div>
                    <div class="desc">
                        <p>{{$cup->about}}</p>
                        <p class="gold">Поддержи свою команду!</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<script>
    $('.competitions .image').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        autoplay: true,
        autoplayspeed: 2000,
        fade: true
    })
</script>

<section>
    <div class="container">
        <div class="white-content">
            <h2>Информация о кубке</h2>
            <div class="banner">
                <img src="{{$cup->information_image}}">
            </div>
            <div class="plain-text">
                <p>{{$cup->information}}</p>
            </div>
        </div>
    </div>
</section>

@forelse($results as $result)
    <section>
        <div class="container">
            <div class="white-content">
                <h2>{{$result[0]}}</h2>
                {{$result[1]}}
                <p><strong>Вольная борьба:</strong> г. Алматы, Алматинская обл., Актюбинская обл., ВКО;</p>
            </div>
        </div>
    </section>
@empty
@endforelse

@if($images)
    <section class="photos">
        <div class="container">
            <div class="white-content">
                <div class="title-block d-flex justify-content-between">
                    <h2>Федерации</h2>
                    <div class="title-block__carousel-controls d-flex">
                        <span class="fas fa-angle-left" onclick="$('.photos__carousel').trigger('prev.owl.carousel', [500]);"></span>
                        <span class="fas fa-angle-right" onclick="$('.photos__carousel').trigger('next.owl.carousel', [500]);"></span>
                    </div>
                </div>
                <div class="photos__carousel owl-carousel">
                    @foreach($images as $image)
                        <a href="{{$image}}" data-fancybox="photos"><img src="{{$image}}"></a>
                    @endforeach
                </div>
            </div>
        </div>
    </section>
@endif

<script>
    var owl = $('.photos__carousel');
    owl.owlCarousel({
//        dots: true,
        autoWidth:true,
        nav: false,
        margin: 30,
        smartSpeed: 800,
        responsive: {
            1600: {
                items: 4
            },
            1024: {
                items: 4
            },
            768: {
                items: 3,
                margin: 15,
                autoWidth: false
            },
            600: {
                items: 2,
                margin: 10,
                autoWidth: false
            },
            0: {
                items: 1,
                autoWidth: false
            }
        }
    });
    owl.on('mousewheel', '.owl-stage', function (e) {
        if (e.deltaY > 0) {
            $(this).trigger('next.owl');
        } else {
            $(this).trigger('prev.owl');
        }
        e.preventDefault();
    });
</script>

@endsection

