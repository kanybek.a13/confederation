@extends('layouts.app')
@section('main')

    <section>
    <div class="container">
        <div class="white-content">
            <div class="title-breadcrumbs">
                <h2>Документы</h2>
                <ul class="breadcrumbs">
                    <li><a href="/{{$currentFederation->site}}">Главная</a></li>
                    <li><span>Документы</span></li>
                </ul>
            </div>

            <div class="row margin">
                <div class="col-lg-4">
                    <div class="input-group">
                        <form action="/confederation/documents" method="get">
                            <select name="federation_id" data-placeholder="Все виды спорта" class="chosen">
                                <option selected>Все виды спорта</option>
                                @forelse($allFederations as $federation)
                                    <option {{ request('federation_id') === $federation->id ? 'selected' : '' }} value="{{$federation->id}}">{{$federation->sport}}</option>
                                @endforeach
                            </select>
                        </form>
                    </div>
                </div>
            </div>

            @forelse($documents as $document)
                <a href="#" download class="card-regular">
                    <img class="icon" src="/assets/img/file.png">
{{--                <img class="icon" src="/assets/img/file-download.png">--}}
                    <div>
                        <h3 class="title">{{$document->name}}</h3>
                        <p class="grey">{{$document->created_at}}</p>
                    </div>
                    <div class="d-flex align-items-center download-details">
                        <i class="flaticon-down"></i><span>docx, 1.26M</span>
                    </div>
                </a>
            @empty
                <p>No files yet!</p>
            @endforelse
        </div>
    </div>
</section>

@endsection

