@extends('layouts.app')
@section('main')

    <div class="main-wrapper">
        <section class="news">
            <div class="container">
                <div class="white-content">
                    <div class="title-breadcrumbs">
                        <h2>Календарь событий</h2>
                        <ul class="breadcrumbs">
                            <li><a href="/{{$currentFederation->site}}">Главная</a></li>
                            <li><span>Календарь событий</span></li>
                        </ul>
                    </div>

                    <div class="list row">
                        @forelse($events as $event)
                            <div class="col-lg-6">
                                <a href="/{{$currentFederation->site}}/event/{{$event->id}}" class="d-flex align-items-center">
                                    <img src="{{$event->image}}">
                                    <div>
                                        <h3>{{$event->name}}</h3>
                                        <p class="meta">{{$event->start_date}}
                                            - {{$event->finish_date}}</p>
                                    </div>
                                </a>
                            </div>
                        @empty
                            No events yet!
                        @endforelse
                    </div>

                    <ul class="pagination">
                        <li class="previous_page"><a {{$events->currentPage() == 1 ? 'disabled' : '' }} href="{{request()->getRequestUri()}}?page={{ $events->currentPage() - 1 }}"><i class="fas fa-long-arrow-alt-left"></i></a></li>

                        @for($page = 1; $page <= $events->lastPage(); $page++)
                            <li><a {{ $page === $events->currentPage() ? 'class="active"' : '' }} href="/{{$currentFederation->site}}/events?page={{$page}}">{{ $page }}</a></li>
                        @endfor

                        <li class="next_page"><a {{$events->currentPage() === $events->lastPage() ? 'disabled' : '' }} href="/{{$currentFederation->site}}/events?page={{ $events->currentPage() + 1 }}"><i class="fas fa-long-arrow-alt-right"></i></a></li>
                    </ul>
                </div>
            </div>
        </section>

@endsection
