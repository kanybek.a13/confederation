@extends('layouts.app')
@section('main')
    <div class="main-wrapper">
        <article>
            <div class="container">
                <div class="white-content">
                    <div class="title-block d-flex justify-content-between">
                        <div>
                            <h1>{{$event->name}}</h1>
                            <div class="date">{{$event->start_date}}
                                - {{$event->finish_date}}</div>
                        </div>
                    </div>
                    <div class="plain-text">
                        <p>
                            <span style="line-height:1.45em;">{{$event->location}}</span>
                        </p>
                        <p>
                            {{$event->about}}
                        </p>
                    </div>
                </div>
            </div>
        </article>

@endsection
