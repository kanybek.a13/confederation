@extends('layouts.app')
@section('main')

<section class="executive-committee">
    <div class="container">
        <div class="white-content">
            <div class="title-breadcrumbs">
                <h2>Исполнительный комитет</h2>
                <ul class="breadcrumbs">
                    <li><a href="/{{$currentFederation->site}}">Главная</a></li>
                    <li><span>Руководство</span></li>
                </ul>
            </div>

            @forelse($governances as $governance)
                <div class="card-regular">
                    <img class="img" src="{{$governance->image}}">
                    <div>
                        <h3 class="title">{{$governance->fullname}}</h3>
                        <p class="grey"><strong>{{$governance->position}}</strong></p>
                    </div>
                </div>
            @empty
                No governance yet!
            @endforelse

        </div>
    </div>
</section>

@endsection
