@extends('layouts.header')
@section('content')
    @yield('main')

    @include('layouts.partners-section')
    @include('layouts.footer')
@endsection
