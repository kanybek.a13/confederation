@if ($errors->{ $bag ?? 'default' }->any())
    <ul class="field mt-6 list-reset">
        @foreach ($errors->{ $bag ?? 'default' }->all() as $error)
            <li class="sm:text-xs text-red">{{ $error }}</li>
        @endforeach
    </ul>
@endif
