<!DOCTYPE html>
<html lang="en">

@include('layouts.head')

<body>
<header>
    <div class="header__bottom">
        <div class="container">
            <div class="d-flex flex-wrap flex-lg-nowrap justify-content-between">
                <a href="/{{$currentFederation->site}}" class="header__logo">
                    <img src="{{$currentFederation->logo}}">
                    <span>{{$currentFederation->name}}</span>
                </a>
                <div class="header__sports">
                    @forelse($allFederations as $federation)
                        @if($currentFederation->site === $federation->site) @continue @endif
                        <a href="/{{$federation->site}}"><img src="{{$federation->logo}}"></a>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
    <div class="header__top">
        <div class="container">
            <div class="d-flex justify-content-between align-items-center">
                <div class="left-block">
                    <a href="#">{{__('app.contacts')}}</a>
                    <a href="#"><i class="fab fa-instagram"></i></a>
                    <a href="#"><i class="fab fa-twitter"></i></a>
                    <a href="#"><i class="fab fa-facebook-f"></i></a>
                    <a href="http://olympic.kz" target="_blank" class="mob-hidden">#TEAMKZ</a>
                </div>
                <div class="right-block">
                    <div class="lang">
                        <a href="{{route('locale', 'kk')}}" title="Қаз" class="{{session('locale') === 'kk' ? 'active' : ''}}">Қаз</a>
                        <a href="{{route('locale', 'ru')}}" title="Рус" class="{{session('locale') === 'ru' ? 'active' : ''}}">Рус</a>
                        <a href="{{route('locale', 'en')}}" title="Eng" class="{{session('locale') === 'en' ? 'active' : ''}}">Eng</a>
                    </div>
                    <a href="javascript:void(0)" onclick="$('.header__search').fadeIn()">
                        <i class="fas fa-search"></i>
                    </a>
                    <div class="menu-btn">
                        <div class="nav-icon" onclick="$(this).toggleClass('opened'); $('.menu').toggleClass('active')">
                            <span></span>
                            <span></span>
                            <span></span>
                            <span></span>
                        </div>
                    </div>
                </div>
            </div>

            <form class="header__search">
                <div class="d-flex">
                    <input type="text" name="search">
                    <button type="submit"><i class="fas fa-search"></i></button>
                    <span onclick="$('.header__search').fadeOut()">&#10006;</span>
                </div>
            </form>
        </div>
    </div>
    <div class="menu">
        <div class="container">
            <nav>
                <ul class="mob-menu">
                    <li class="dropdown">
                        <a href="javascript:void(0)">{{__('app.federations')}}</a>
                        <div class="dropdown-menu">
                            <ul class="container">
                                @forelse($allFederations as $federation)
                                    <li><a href="/{{$federation->site}}">{{$federation->sport}}</a></li>
                                @empty
                                    <p>No federations yet!</p>
                                @endforelse
                            </ul>
                        </div>
                    </li>
                </ul>
                <ul>
                    <li class="dropdown">
                        <a href="/{{$currentFederation->site}}/goals">{{__('app.about_federation')}}</a>
                        <div class="dropdown-menu">
                            <ul class="container">
                                <li><a href="/{{$currentFederation->site}}/governance">{{__('app.governance')}}</a></li>
                                <li><a href="/{{$currentFederation->site}}/structure">{{__('app.structure')}}</a></li>
                                <li><a href="/{{$currentFederation->site}}/history">{{__('app.history')}}</a></li>

                                @if($currentFederation->site==='confederation')
                                    <li><a href="/{{$currentFederation->site}}/results">{{__('app.documents')}}</a></li>
                                @endif
                            </ul>
                        </div>
                    </li>

                    @if($currentFederation->site=='confederation')
                        <li class="dropdown">
                            <a href="javascript:void(0)">{{__('app.federations')}}</a>
                            <div class="dropdown-menu">
                                <ul class="container">
                                    @forelse($allFederations as $federation)
                                        <li>
                                            <a href="/{{$federation->site}}">{{$federation->sport}}</a>
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                        </li>
                    @endif

                    <li class="dropdown">
                        <a href="/{{$currentFederation->site}}/news" class="scrollto">{{__('app.news')}}</a>
                    </li>
                    <li class="dropdown">
                        <a href="/{{$currentFederation->site}}/media">{{__('app.media')}}</a>
                    </li>

                    @if($currentFederation->site=='confederation')
                        <li class="dropdown">
                            <a href="javascript:void(0)">{{__('app.confederation_cup')}}</a>
                            <div class="dropdown-menu">
                                <ul class="container">
                                    @forelse($cups as $cup)
                                        <li>
                                            <a href="/{{$currentFederation->site}}/cup/{{$cup->id}}">{{$cup->name}}</a>
                                        </li>
                                    @empty
                                        No cups yet!
                                    @endforelse
                                </ul>
                            </div>
                        </li>
                    @elseif($currentFederation->site!=='confederation')
                        <li class="dropdown">
                            <a href="/{{$currentFederation->site}}/events" class="scrollto">{{__('app.events')}}</a>
                        </li>
                        <li class="dropdown">
                            <a href="/{{$currentFederation->site}}/results" class="scrollto">{{__('app.results')}}</a>
                        </li>
                        <li class="dropdown">
                            <a href="/{{$currentFederation->site}}/teams" class="scrollto">{{__('app.teams')}}</a>
                        </li>
                        <li class="dropdown">
                            <a href="/{{$currentFederation->site}}/coaches" class="scrollto">{{__('app.coaches')}}</a>
                        </li>
                        <li class="dropdown">
                            <a href="/{{$currentFederation->site}}/sections" class="scrollto">{{__('app.sections')}}</a>
                        </li>
                    @endif

                </ul>
                <ul class="mob-menu">
                    <li><a href="#">{{__('app.contacts')}}</a></li>
                </ul>
                <div class="mob-langs d-lg-none">
                    <a href="{{route('locale', 'kk')}}" title="Қаз" class="{{session('locale') === 'kk' ? 'active' : ''}}">Қаз</a> |
                    <a href="{{route('locale', 'ru')}}" title="Рус" class="{{session('locale') === 'ru' ? 'active' : ''}}">Рус</a> |
                    <a href="{{route('locale', 'en')}}" title="Eng" class="{{session('locale') === 'en' ? 'active' : ''}}">Eng</a> |
                </div>
            </nav>
        </div>
    </div>
</header>

<div class="main-wrapper">

<!--Main part-->
    @yield('content')
<!---->
