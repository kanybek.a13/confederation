@extends('layouts.app')
@section('main')

<section>
    <div class="container">
        <div class="white-content">
            <div class="title-breadcrumbs">
                <h2>{{$media->name}}</h2>
                <ul class="breadcrumbs">
                    <li><a href="/{{$currentFederation->site}}">Главная</a></li>
                    <li><a href="/{{$currentFederation->site}}/media">Медиа</a></li>
                    <li><span>{{$media->name}}</span></li>
                </ul>
            </div>

            <div class="gallery">
                <div class="slider-for">
                    @forelse($photos as $photo)
                        <a href="{{$photo->path}}" data-fancybox="photo"><img src="{{$photo->path}}"></a>
                    @empty
                        <p>No photos yet!</p>
                    @endforelse
                </div>
                <div class="slider-nav">
                    @forelse($photos as $photo)
                        <img src="{{$photo->path}}">
                    @empty
                        <p>no photos yet!</p>
                    @endforelse
                </div>
            </div>
        </div>
    </div>
</section>

<script>
    $('.slider-for').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        fade: true,
        asNavFor: '.slider-nav'
    });
    $('.slider-nav').slick({
        slidesToShow: 5,
        slidesToScroll: 1,
        asNavFor: '.slider-for',
        centerMode: true,
        centerPadding: '0px',
        focusOnSelect: true,
        prevArrow: '<i class="fas fa-angle-left"></i>',
        nextArrow: '<i class="fas fa-angle-right"></i>',
        responsive: [
            {
                breakpoint: 1025,
                settings: {
                    slidesToShow: 4
                }
            },
            {
                breakpoint: 768,
                settings: {
                    slidesToShow: 3
                }
            },
            {
                breakpoint: 575,
                settings: {
                    slidesToShow: 2
                }
            }
        ]
    });
</script>

@endsection
