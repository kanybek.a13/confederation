@extends('layouts.app')
@section('main')

<section class="news">
    <div class="container">
        <div class="white-content">
            <div class="title-breadcrumbs">
                <h2>Новсти</h2>
                <ul class="breadcrumbs">
                    <li><a href="/{{$currentFederation->site}}">Главная</a></li>
                    <li><a href="/{{$currentFederation->site}}/news">Новости</a></li>
                    <li><span>{{$news->title}}</span></li>
                </ul>
            </div>

            <div class="container">
                <div class="white-content">
                    <div class="title-block d-flex justify-content-between">
                        <div>
                            <h1>{{ $news->title }}</h1>
                            <div class="date">{{ $news->created_at }}</div>
                        </div>
                    </div>

                    <div class="image" style="padding: 0;">
                        <a href="{{ $news->files[0]->path }}" data-fancybox=""><img src="{{ $news->files[0]->path }}" style="position: initial;"></a>
                    </div>
                    <div class="plain-text">
                        <p style="text-align: justify;"><strong>{{ $news->title }}</strong></p>
                        <blockquote>
                            <p style="text-align: justify;">{{ $news->body }}.</p>
                        </blockquote>

                        <p style="text-align: justify;"><strong>Источник: Казахстанская федерация бокса</strong></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
