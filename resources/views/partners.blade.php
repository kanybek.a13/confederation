@extends('layouts.app')
@section('main')

<section>
    <div class="container">
        <div class="white-content">
            <div class="title-breadcrumbs">
                <h2>Спонсоры и партнеры</h2>
                <ul class="breadcrumbs">
                    <li><a href="/{{$currentFederation->site}}">Главная</a></li>
                    <li><span>Спонсоры и партнеры</span></li>
                </ul>
            </div>
            <h3 class="h-secondary">Титульные спонсоры</h3>
            <div class="row">
                @forelse($partners as $partner)
                    @if($partner->type==='Титульный спонсор')
                        <div class="col-md-3 align-self-center text-center">
                            <img src="{{$partner->image}}" style="margin: 2em 0;">
                        </div>
                        <div class="col-md-9">
                            <h4>{{$partner->name}}</h4>
                            <p style="color: #aaa; font-weight: 500">Веб-сайт: <a href="{{$partner->site}}">{{$partner->site}}</a></p>
                            <p>{{$partner->about}}</p>
                        </div>
                        @break
                    @endif
                @endforeach
            </div>

            <h3 class="h-secondary">Сотрудничество</h3>
            <div class="partners just-links">
                <div class="row">
                    @forelse($partners as $partner)
                        @if($partner->type==='Сотрудничество')
                            <div class="col-lg-3 col-sm-6">
                                <a href="{{$partner->site}}"><img src="{{$partner->image}}"></a>
                            </div>
                        @endif
                    @endforeach
                </div>
            </div>

            <h3 class="h-secondary">Спонсоры</h3>
            <div class="partners just-links">
                <div class="row">
                    @forelse($partners as $partner)
                        @if($partner->type==='Спонсор')
                            <div class="col-lg-3 col-sm-6">
                                <a href="{{$partner->site}}"><img src="{{$partner->image}}"></a>
                            </div>
                        @endif
                    @endforeach
                </div>
            </div>

            <h3 class="h-secondary">Партнеры</h3>
            <div class="partners just-links">
                <div class="row">
                    @forelse($partners as $partner)
                        @if($partner->type==='Партнер')
                            <div class="col-lg-3 col-sm-6">
                                <a href="{{$partner->site}}"><img src="{{$partner->image}}"></a>
                            </div>
                        @endif
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</section>

@endsection
