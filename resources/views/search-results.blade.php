@extends('layouts.app')
@section('main')

<section>
    <div class="container">

        <div class="white-content">
            <div class="title-breadcrumbs">
                <h2>Результаты поиска</h2>
                <ul class="breadcrumbs">
                    <li><a href="/{{$currentFederation->site}}">Главная</a></li>
                    <li><span>Результаты поиска</span></li>
                </ul>
            </div>
            <form>
                <div class="input-group">
                    <input type="text" name="search">
                    <button style="width: auto;flex-shrink: 0; margin-left: 1em" type="submit" class="button-primary"><i class="fas fa-search"></i></button>
                </div>
            </form>
            <p class="inline-message">Всего найдено: 43</p>

            <div class="search-result-list">
                <a href="#">
                    <h4>Майра Бакашева: "Спортсмены стали более осознанными"</h4>
                    <p>В минувший четверг на базе Всемирной Академии Бокса AIBA (Алматинская область) прошел антидопинговый семинар для мужской сборной РК по тяжелой атлетике. Организатором выступил Казахстанский Национальный антидопинговый центр...</p>
                    <span class="button-secondary">Перейти</span>
                </a>
                <a href="#">
                    <h4>В Конфедерации спортивных единоборств
                        и силовых видов спорта назначен новый...</h4>
                    <p>Обсуждали насущные проблемы, говорили о новых стандартах, в том числе в сфере образования. Отдельное внимание было уделено спортивному питанию и последствиям, которые влечет за собой его неосторожное применение...</p>
                    <span class="button-secondary">Перейти</span>
                </a>
            </div>

            <ul class="pagination">
                <li class="previous_page disabled"><a href="#"><i class="fas fa-long-arrow-alt-left"></i></a></li>
                <li class="active"><a href="#">1</a></li>
                <li><a href="#">2</a></li>
                <li><a href="#">3</a></li>
                <li><a href="#">4</a></li>
                <li><a href="#">5</a></li>
                <li><a href="#">6</a></li>
                <li><a href="#">7</a></li>
                <li><a href="#">8</a></li>
                <li class="dots disabled"><a href="#">...</a></li>
                <li><a href="#">498</a></li>
                <li><a href="#">499</a></li>
                <li class="next_page"><a href="#"><i class="fas fa-long-arrow-alt-right"></i></a></a></li>
            </ul>
        </div>
    </div>
</section>

@endsection
