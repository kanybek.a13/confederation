@extends('layouts.app')
@section('main')

    <section class="news">
        <div class="container">
            <div class="white-content">
                <div class="title-breadcrumbs">
                    <h2>{{$section->name}}</h2>
                    <ul class="breadcrumbs">
                        <li><a href="/{{$currentFederation->site}}">Главная</a></li>
                        <li><a href="/{{$currentFederation->site}}/sections">Спортивные секции</a></li>
                        <li><span>{{$section->name}}</span></li>
                    </ul>
                </div>

                <div class="news__main">
                    <div class="item d-flex">
                        <div class="image">
                            <img src="{{$section->image}}">
                        </div>
                        <div class="desc">
                            <div class="annotation">
                                <p class="grey"> {{$section->address}} </p>
                                <p class="grey"> {{$section->phone}} </p>
                                <p>
                                    <span style="line-height:1.45em;"> {{$section->name}} </span>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection
