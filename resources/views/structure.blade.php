@extends('layouts.app')
@section('main')

<div class="container">
    <div class="white-content">
        <div class="title-breadcrumbs">
            <h2>Структура и основные направления деятельности</h2>
            <ul class="breadcrumbs">
                <li><a href="{{$currentFederation->site}}">Главная</a></li>
                <li><span>Структура и основные направления деятельности</span></li>
            </ul>
        </div>
        @if($structure)

            <div class="plain-text">
                <h4>{{$structure->title}}</h4>
                <div class="banner">
                    <img src="{{$structure->image}}">
                </div>

                <p>{{$structure->body}}</p>
            </div>
        @else
            No structure yet!
        @endif
    </div>
</div>

@endsection
